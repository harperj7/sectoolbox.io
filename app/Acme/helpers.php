<?php

function set_active($path, $state = 'active') {

	return Request::is($path) ? $state : '';
}

function errors_for($attribute, $errors) {

	return $errors->first($attribute, '<br /><div class="alert alert-danger" role="alert">:message</div>');

}

function get_breadcrumb() {

	return ucfirst(Request::path());
}

function time_difference ($scans) {

	$time_taken_array = [];

	foreach ($scans as $scan) {
		
		$started = new DateTime($scan->started);
		$finished = new DateTime($scan->finished);
		$interval = $finished->diff($started);
		$time_taken = $interval->format('%hh %im %Ss');
		$time_taken_array[] = $time_taken;

	}

	return $time_taken_array;

}
<?php

class CustomValidationRules extends Illuminate\Validation\Validator{

    public function validateBlacklistIp($attribute, $value, $parameters)
    {
        $ip = strip_tags($value);

        $pri_addrs = array(
            '10.0.0.0|10.255.255.255',
            '172.16.0.0|172.31.255.255',
            '192.168.0.0|192.168.255.255',
            '169.254.0.0|169.254.255.255',
            '127.0.0.0|127.255.255.255',
            '104.156.250.181|104.156.250.181'
        );

        $long_ip = ip2long($ip);

        if ($long_ip !== false) {

            foreach($pri_addrs as $pri_addr) {
                list($start, $end) = explode('|', $pri_addr);

                // If it is contained in the array
                if ($long_ip >= ip2long($start) && $long_ip <= ip2long($end)) {
                    return false;
                }
            }
        } else {
            return false;
        }

        return true;
    }

    public function validateUrlWithScheme($attribute, $value, $parameters)
    {
        $url = strip_tags($value);

        if (preg_match('~((http|https)://|www.)(.+?)~', $url)) {
            return true;
        }

        return false;
    }
    public function validateIpAddress($attribute, $value, $parameters)
    {
        $ip = strip_tags($value);

        return filter_var($ip, FILTER_VALIDATE_IP, ['flags' => 'FILTER_FLAG_IPV4, FILTER_FLAG_IPV6, FILTER_FLAG_NO_PRIV_RANGE, FILTER_FLAG_NO_RES_RANGE']);
    }
    public function validateUrlOrIp($attribute, $value, $parameters)
    {
        $target = strip_tags($value);

        $ip_valid = $this->validateIpAddress(null, $target, null);

        if ($ip_valid === false) {

            $url = parse_url($target);

            if ($url === false) {
                return false;
            }

        } else {

            return $this->validateBlacklistIp(null, $target, null);

        }

        return true;

    }

}
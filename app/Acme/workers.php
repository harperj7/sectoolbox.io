<?php
use PhpAmqpLib\Connection\AMQPConnection;
use PhpAmqpLib\Message\AMQPMessage;

class Workers {

	public function createConnection() {

		try {

	        // return $connection = new AMQPConnection('192.168.0.17', 5672, 'front_end', 'moardata77');
					return $connection = new AMQPConnection($_ENV['RABBIT_HOST'], $_ENV['RABBIT_PORT'], $_ENV['RABBIT_USER'], $_ENV['RABBIT_PASS']);

	    } catch (AMQPException $exception) {

	        return Redirect::route('console');

	    }

	}

	public function createChannel($connection) {

		try {

			return $channel = $connection->channel();

		} catch (AMQPConnectionException $exception) {

			return Redirect::route('console');

		}

	}

	public function createExchange($channel, $connection, $type = 'direct', $name = '') {

		try {

			$exchange = new AMQPExchange($channel);
			$exchange->setName($name);
			$exchange->setType($type);
			$exchange->declareExchange();

			return $exchange;

		} catch (AMQPExchangeException $exception) {

			return Redirect::route('console');

		} catch (AMQPConnectionException $exception) {

			return Redirect::route('console');

		}

	}

	public function createQueue($channel, $queue_name) {

		try	{

			return $channel->queue_declare($queue_name, true);

		} catch (AMQPQueueException $exception)	{

			return Redirect::route('console');

		}

	}

	public function sendMessage ($channel, $queue, $data) {

        $jsonMessage = json_encode($data);

		try {

			$msg = new AMQPMessage($jsonMessage);

			return $channel->basic_publish($msg, '', $queue);

		} catch (AMQPExchangeException $exception) {

			return Redirect::route('console');

		} catch (AMQPConnectionException $exception) {

			return Redirect::route('console');

		} catch (AMQPChannelException $exception) {

			return Redirect::route('console');

		}

	}

	public function closeConnection ($connection) {

		return $connection->close();

	}

}

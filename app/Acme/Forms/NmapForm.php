<?php namespace Acme\Forms;

use Illuminate\Support\Facades\Validator;
use Laracasts\Validation\FormValidator;

class NmapForm extends FormValidator {

	protected $rules = [
		'target'    => 'required|url_or_ip',
        'scan_type' => 'required',
        'port_scan_technique' => 'alpha',
        'ports' => 'required',
        'port-comma-list' => 'required_if:ports,port_list|regex:/^[0-9][0-9,][0-9,\s]*[0-9]/',
        'ping' => 'boolean',
        'dns' => 'boolean',
        'version_only' => 'boolean',
        'os' => 'boolean',
        'versionscript' => 'boolean',
        'email-me' => 'required|boolean',
        'terms' => 'required|boolean|regex:/^[1]/',
        'start_port' => 'required_if:ports,port_range|integer',
        'end_port' => 'required_if:ports,port_range|integer'
	];

}
<?php namespace Acme\Forms;

use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Validator;
use Laracasts\Validation\FormValidator;

Validator::extend('current_password', function($attribute, $value, $parameters)
{
    $user = Auth::user();

    if (!empty($value) && Hash::check($value, $user->password)) {
        return true;
    }
    return false;
});

class ChangePasswordForm extends FormValidator {

    protected $rules = [
        'current-pass'    => 'required|current_password|min:8',
        'password' => 'required|confirmed|min:8'
    ];

}

<?php namespace Acme\Forms;

use Illuminate\Support\Facades\Validator;
use Laracasts\Validation\FormValidator;

class SqlmapForm extends FormValidator {

	protected $rules = [
		'target'    => 'required|url_with_scheme',
        'threads'   => 'required|integer',
        'level'     => 'required|integer',
        'risk'      => 'required|integer',
        'test-forms' => 'required|boolean'
	];

}
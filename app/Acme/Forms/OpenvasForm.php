<?php namespace Acme\Forms;

use Laracasts\Validation\FormValidator;

class OpenvasForm extends FormValidator {

	protected $rules = [
		'target'    => 'required|ip|blacklist_ip'
	];

}
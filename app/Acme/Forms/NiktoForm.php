<?php namespace Acme\Forms;

use Illuminate\Support\Facades\Validator;
use Laracasts\Validation\FormValidator;

class NiktoForm extends FormValidator {

	protected $rules = [
		'target'    => 'required|url_with_scheme',
		'port' => 'integer|digits_between:0,65535',
        'email-me' => 'required|boolean'
	];

}
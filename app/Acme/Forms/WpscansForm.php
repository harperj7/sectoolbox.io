<?php namespace Acme\Forms;

use Laracasts\Validation\FormValidator;

class WpscansForm extends FormValidator {

	protected $rules = [
		'target'    => 'required|active_url'
	];

}
<?php

Route::when('*', 'force.ssl');

Route::get('/', ['as' => 'home', 'uses' => 'PagesController@showIndex']);
//Route::get('pricing', 'PagesController@showPricing');
Route::get('terms-and-conditions', 'PagesController@showTerms');

Route::group(array('before'=>'guest'), function() {
	//Route::get('register', 'RegistrationController@create');
	//Route::post('register', ['as' => 'registration.store', 'uses' => 'RegistrationController@store']);
	Route::get('login', ['as' => 'login', 'uses' => 'SessionsController@create']);
});


Route::resource('Sessions', 'SessionsController', ['only' => ['create', 'store', 'destroy']]);

//Route::get('nav', function(){
//
//    return View::make('backend.test');
//});

Route::group(array('before'=>'auth'), function() {
    Route::get('console', ['as' => 'console', 'uses' => 'PagesController@showConsole']);
    Route::get('console/logout', ['as' => 'console.logout', 'uses' => 'SessionsController@destroy']);
    Route::resource('console/nikto', 'NiktosController');
    Route::resource('console/nmap', 'NmapsController');
    Route::resource('console/openvas', 'OpenvasController');
    Route::resource('console/sqlmap', 'SqlmapsController');

    Route::get('console/nikto/{nikto}/xml', 'NiktosController@downloadXml');
    Route::get('console/nmap/{nmap}/xml', 'NmapsController@downloadXml');
    Route::get('console/openvas/{openvas}/xml', 'OpenvasController@downloadXml');
    Route::get('console/sqlmap/{sqlmap}/xml', 'SqlmapsController@downloadXml');

    Route::get('console/nikto/{nikto}/html', 'NiktosController@viewHtml');
    Route::get('console/nmap/{nmap}/html', 'NmapsController@viewHtml');
    Route::get('console/openvas/{openvas}/html', 'OpenvasController@viewHtml');
    Route::get('console/sqlmap/{sqlmap}/html', 'SqlmapsController@viewHtml');
    Route::get('console/wpscan/{niktoscan}/html', 'NiktoController@viewHtml');

    Route::get('console/openvas/{openvas}/pdf', 'OpenvasController@downloadPdf');

    Route::get('console/change-password', 'PagesController@showChangePassword');
    Route::put('console/user/update/password', ['as' => 'update.password', 'uses' => 'RegistrationController@updatePassword']);

    Route::get('console/scans', 'PagesController@showUserScans');
});

Route::get('/email', function(){

	Mail::send('emails.test', [], function($message){
		$message->to('jonnyharp@gmail.com')->subject('Sec Toolbox Test Email');
	});

});

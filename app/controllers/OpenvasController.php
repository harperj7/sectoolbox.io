<?php

use Acme\Forms\OpenvasForm;

class OpenvasController extends \BaseController {

	function __construct(OpenvasForm $openvasForm) {

		$this->openvasForm = $openvasForm;

	}

	/**
	 * Display a listing of the resource.
	 * GET /openvas
	 *
	 * @return Response
	 */
	public function index()
	{
		$openvas_scans = Openvas::where('user_id', '=', Auth::id())->paginate(15);

		$time_taken = time_difference($openvas_scans);

		return View::make('backend/scans/openvas/index', array('title' => 'OpenVAS scans'))->with(compact('openvas_scans', 'time_taken'));
	}

	/**
	 * Show the form for creating a new resource.
	 * GET /openvas/create
	 *
	 * @return Response
	 */
	public function create()
	{
		return View::make('backend/scans/openvas/create', array('title' => 'Create an OpenVAS scan'));
	}

	/**
	 * Store a newly created resource in storage.
	 * POST /openvas
	 *
	 * @return Response
	 */
	public function store()
	{
        $input = Input::all();

		$this->openvasForm->validate($input);

        $openvas = new Openvas;
        $previous_id = $openvas->orderBy('id', 'DESC')->pluck('id');
        $previous_id = ($previous_id !== null) ? ($previous_id + 1) : 1;

        $data = [
            'target' => $input['target'],
            'email_result' => 0,
            'IndexID' => $previous_id,
            'UserID' => Auth::id(),
            'email' => Auth::user()->email,
            'scantype' => 'openvas'
        ];

		$worker = new Workers;
		$connection = $worker->createConnection();
		$channel 	= $worker->createChannel($connection);
		$queue 		= $worker->createQueue($channel, 'openvas_queue');
		$message  	= $worker->sendMessage($channel, 'openvas_queue', $data);
		$closed 	= $worker->closeConnection($connection);

        $openvas->message_sent = json_encode($data);
		$openvas->user_id = Auth::id();
		$openvas->target = $input['target'];
		$openvas->completed = 0;

		$openvas->save();

		return Redirect::back()->with('message', 'OpenVAS scan successfully created');
	}

	/**
	 * Display the specified resource.
	 * GET /openvas/{id}
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function show($id)
	{
		//
	}

	/**
	 * Show the form for editing the specified resource.
	 * GET /openvas/{id}/edit
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function edit($id)
	{
		//
	}

	/**
	 * Update the specified resource in storage.
	 * PUT /openvas/{id}
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function update($id)
	{
		//
	}

	/**
	 * Remove the specified resource from storage.
	 * DELETE /openvas/{id}
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function destroy($id)
	{
		$openvas_scan = Openvas::find($id);

		$openvas_scan->delete();

		return Redirect::back()->with('openvasmessage', 'OpenVAS scan successfully deleted');
	}

	public function downloadXml($id) {

        $openvas_scan_xml= User::find(Auth::id())->openvas_scans()->where('id', '=', $id)->first();

        if ($openvas_scan_xml !== null) {
            $files = json_decode($openvas_scan_xml->results, true);
            return (Response::download($files['xml']));
        }
        return Redirect::back();
	}

    public function downloadPdf($id) {

        $openvas_scan_pdf = User::find(Auth::id())->openvas_scans()->where('id', '=', $id)->first();
        if ($openvas_scan_pdf !== null) {
            $files = json_decode($openvas_scan_pdf->results, true);
            return (Response::download($files['pdf']));
        }
        return Redirect::back();
    }

    public function viewHtml($id) {

        $openvas_scan_xml= User::find(Auth::id())->openvas_scans()->where('id', '=', $id)->first();

        if ($openvas_scan_xml !== null) {
            $xmlDoc = new DOMDocument();
            $files = json_decode($openvas_scan_xml->results, true);
            $xmlDoc->load($files['xml']);

            $xslDoc = new DOMDocument();
            $xslDoc->load('/var/www/securitytoolbox.io/app/openvas_html_stylesheet.xsl');

            $processor = new XSLTProcessor();
            $processor->importStylesheet($xslDoc);
            $html_results = $processor->transformToXML($xmlDoc);

            return View::make('backend/scans/openvas/html', array('title' => 'OpenVAS scan results', 'html_results' => $html_results));
        }
        return View::make('backend/scans/unauthorised', ['title' => 'Unauthorised access']);
    }

}
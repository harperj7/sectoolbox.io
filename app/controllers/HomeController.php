<?php

class HomeController extends BaseController {

	public function showIndex() {

		return View::make('frontend/index');
	}

	public function showPricing() {

		return View::make('frontend/pricing', array('title' => 'Pricing'));
	}

	public function showRegistration() {

		return View::make('frontend/registration/index', array('title' => 'Register'));
	}

}

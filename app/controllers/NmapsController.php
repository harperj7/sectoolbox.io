<?php

use Acme\Forms\NmapForm;

class NmapsController extends \BaseController {

	function __construct(NmapForm $nmapForm) {

		$this->nmapForm = $nmapForm;

	}

	/**
	 * Display a listing of the resource.
	 * GET /nmaps
	 *
	 * @return Response
	 */
	public function index()
	{
		$nmap_scans = Nmap::where('user_id', '=', Auth::id())->paginate(15);

		$time_taken = time_difference($nmap_scans);

		return View::make('backend/scans/nmap/index', array('title' => 'Nmap scans'))->with(compact('nmap_scans', 'time_taken'));
	}

	/**
	 * Show the form for creating a new resource.
	 * GET /nmaps/create
	 *
	 * @return Response
	 */
	public function create()
	{
		return View::make('backend/scans/nmap/create', array('title' => 'Create an Nmap scan'));
	}

	/**
	 * Store a newly created resource in storage.
	 * POST /nmaps
	 *
	 * @return Response
	 */
	public function store()
	{
		$input = Input::all();

        if (strpos($input['target'], 'http://') === 0) {
            $input['target'] = str_replace('http://', '', $input['target']);
        } elseif (strpos($input['target'], 'https://') === 0) {
            $input['target'] = str_replace('https://', '', $input['target']);
        }

		$this->nmapForm->validate($input);

        $ssl = ($input['scan_type'] === 'nmap_ssl') ? true : false;

        $options = $this->build_options($input, $ssl);

        $nmap = new Nmap;
        $previous_id = $nmap->orderBy('id', 'DESC')->pluck('id');
        $previous_id = ($previous_id !== null) ? ($previous_id + 1) : 1;

        if (isset($input['services_scripts']) && $input['services_scripts'] === 'version_only') {
            $versionscript = 'version_only';
        } elseif(isset($input['services_scripts']) && $input['services_scripts'] === 'version_scripts') {
            $versionscript = 'versionwithscripts';
        } else {
            $versionscript = 'none';
        }
        $portrange = ($input['ports'] != 'port_range') ? array() : [$input['start_port'], $input['end_port']];
        $port_list = ($input['ports'] != 'port_list') ? array() : $input['port_comma_list'];
        $ping = (isset($input['ping'])) ?: 0;
        $dns = (isset($input['dns'])) ?: 0;
        $os = (isset($input['os'])) ?: 0;

        $target = strip_tags($input['target']);

        $data = [
            'target' => $target,
            'ipv6' => '0',
            'scantype' => $input['scan_type'],
            'ping' => $ping,
            'dns' => $dns,
            'os' => $os,
            'versionnscript' => $versionscript,
            'email_result' => 0,
            'IndexID' => $previous_id,
            'UserID' => Auth::id(),
            'email' => Auth::user()->email,
            'port_range' => $portrange,
            'port_list' => $port_list,
            'ports' => $input['ports']
        ];

        if ($input['scan_type'] === 'nmap_tcp') {
            $data['port_scan_type'] = $input['port_scan_technique'];
        }

		$worker = new Workers;
		$connection = $worker->createConnection();
		$channel 	= $worker->createChannel($connection);
		$queue 		= $worker->createQueue($channel, 'nmap_queue');
		$message  	= $worker->sendMessage($channel, 'nmap_queue', $data);
		$closed 	= $worker->closeConnection($connection);

		$nmap->message_sent = json_encode($data);
        $nmap->options = $options;
		$nmap->user_id = $data['UserID'];
		$nmap->target = $target;
		$nmap->completed = 0;

		$nmap->save();

		return Redirect::back()->with('message', 'Nmap scan successfully created');
	}

	/**
	 * Display the specified resource.
	 * GET /nmaps/{id}
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function show($id)
	{
		//
	}

	/**
	 * Show the form for editing the specified resource.
	 * GET /nmaps/{id}/edit
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function edit($id)
	{
		//
	}

	/**
	 * Update the specified resource in storage.
	 * PUT /nmaps/{id}
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function update($id)
	{
		//
	}

	/**
	 * Remove the specified resource from storage.
	 * DELETE /nmaps/{id}
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function destroy($id)
	{

        $nmap_scan = User::find(Auth::id())->nmap_scans()->where('id', '=', $id)->first();

        if ($nmap_scan !== null) {
            $nmap_scan->delete();
            return Redirect::back()->with('nmapmessage', 'Nmap scan successfully deleted');
        }
        return Redirect::back();
	}

	public function downloadXml($id)
    {
        $nmap_scan_xml = User::find(Auth::id())->nmap_scans()->where('id', '=', $id)->first();

        if ($nmap_scan_xml !== null) {
            return (Response::download($nmap_scan_xml->results));
        }
        return Redirect::back();
    }

    public function viewHtml($id) {

        $nmap_scan_xml = User::find(Auth::id())->nmap_scans()->where('id', '=', $id)->first();

        if ($nmap_scan_xml !== null) {
            $xmlDoc = new DOMDocument();
            $xmlDoc->load($nmap_scan_xml->results);

            $xslDoc = new DOMDocument();
            $xslDoc->load('/var/www/securitytoolbox.io/app/nmap_html_stylesheet.xsl');

            $processor = new XSLTProcessor();
            $processor->importStylesheet($xslDoc);
            $html_results = $processor->transformToXML($xmlDoc);

            return View::make('backend/scans/nmap/html', array('title' => 'Nmap scan results', 'html_results' => $html_results));
        }
        return View::make('backend/scans/unauthorised', ['title' => 'Unauthorised access']);
    }

    public function build_options($input, $ssl = false)
    {
        $options = [];

        if (!$ssl) {
            if ($input['port_scan_technique'] === 'synscan') {
                $options[] = '-sS';
            } elseif ($input['port_scan_technique'] === 'tcpconnectscan') {
                $options[] = '-sT';
            }
            if ($input['ping'] === '1') {
                $options[] = '-Pn';
            }

            if ($input['dns'] === '1') {
                $options[] = '-n';
            }

            if ($input['os'] === '1') {
                $options[] = '-O';
            }

            if ($input['services_scripts'] === 'version_only') {
                $options[] = '-sV';
            } elseif ($input['services_scripts'] === 'version_scripts') {
                $options[] = '-sV -sC';
            }
        }
        if ($input['ports'] === 'top_100') {
            $options[] = '--top-ports 100';
        } elseif ($input['ports'] === 'top_1000') {
            $options[] = '--top-ports 1000';
        } elseif ($input['ports'] === 'top_2500') {
            $options[] = '--top-ports 2500';
        } elseif ($input['ports'] === 'all_ports') {
            $options[] = '-p 1-65535';
        } elseif ($input['ports'] === 'port_range') {
            $options[] = '-p ' . $input['start_port'] . '-' . $input['end_port'];
        } elseif ($input['ports'] === 'port_list') {
            $list = preg_replace('/\s+/', '', $input['port-comma-list']);
            $options[] = '-p ' . trim($list);
        }

        return implode(' ', $options);

    }

}
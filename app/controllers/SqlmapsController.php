<?php

use Acme\Forms\SqlmapForm;

class SqlmapsController extends \BaseController {

	function __construct(SqlmapForm $sqlmapForm) {

		$this->sqlmapForm = $sqlmapForm;

	}

	/**
	 * Display a listing of the resource.
	 * GET /sqlmaps
	 *
	 * @return Response
	 */
	public function index()
	{
		$sqlmap_scans = Sqlmap::where('user_id', '=', Auth::id())->paginate(15);

		$time_taken = time_difference($sqlmap_scans);

		return View::make('backend/scans/sqlmap/index', array('title' => 'SQLMap scans'))->with(compact('sqlmap_scans', 'time_taken'));
	}

	/**
	 * Show the form for creating a new resource.
	 * GET /sqlmaps/create
	 *
	 * @return Response
	 */
	public function create()
	{
		return View::make('backend/scans/sqlmap/create', array('title' => 'Create an SQLMap scan'));
	}

	/**
	 * Store a newly created resource in storage.
	 * POST /sqlmaps
	 *
	 * @return Response
	 */
	public function store()
	{

        $input = Input::all();

		$this->sqlmapForm->validate($input);
		
		$sqlmap = new Sqlmap;
        $previous_id = $sqlmap->orderBy('id', 'DESC')->pluck('id');
        $previous_id = ($previous_id !== null) ? ($previous_id + 1) : 1;
        $port = (empty($input['port'])) ? '' : htmlentities(strip_tags($input['port']), ENT_QUOTES);

        $target = strip_tags($input['target']);

        $data = [
            'email_result' => 0,
            'IndexID' => $previous_id,
            'UserID' => Auth::id(),
            'email' => Auth::user()->email,
            'scantype' => 'sqlmap',
            'target' => $target,
            'forms' => $input['test-forms'],
            'threads' => $input['threads'],
            'risk' => $input['risk'],
            'level' => $input['level'],
            'port' => $port
        ];

		$worker = new Workers;
		$connection = $worker->createConnection();
		$channel 	= $worker->createChannel($connection);
		$queue 		= $worker->createQueue($channel, 'web_queue');
		$message  	= $worker->sendMessage($channel, 'web_queue', $data);
		$closed 	= $worker->closeConnection($connection);

        $sqlmap->message_sent = json_encode($data);
		$sqlmap->user_id = Auth::id();
		$sqlmap->target = $target;
        $sqlmap->port = (!empty($port)) ?: '80';
		$sqlmap->completed = 0;

		$sqlmap->save();

		return Redirect::back()->with('message', 'SQLMap scan successfully created');
	}

	/**
	 * Display the specified resource.
	 * GET /sqlmaps/{id}
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function show($id)
	{
		//
	}

	/**
	 * Show the form for editing the specified resource.
	 * GET /sqlmaps/{id}/edit
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function edit($id)
	{
		//
	}

	/**
	 * Update the specified resource in storage.
	 * PUT /sqlmaps/{id}
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function update($id)
	{
		//
	}

	/**
	 * Remove the specified resource from storage.
	 * DELETE /sqlmaps/{id}
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function destroy($id)
	{
        $sqlmap_scan = User::find(Auth::id())->sqlmap_scans()->where('id', '=', $id)->first();

        if ($sqlmap_scan !== null) {
            $sqlmap_scan->delete();
            return Redirect::back()->with('sqlmapmessage', 'SQLMap scan successfully deleted');
        }
		return Redirect::back();
	}

    public function viewHtml($id) {

        $sqlmap_scan_xml = User::find(Auth::id())->sqlmap_scans()->where('id', '=', $id)->first();

        if ($sqlmap_scan_xml !== null) {

            $html = '';

            $root = simplexml_load_file($sqlmap_scan_xml->results);
            $base64_zlib = $root->rawoutput[0]->children()[0]['data'];
            $cli_output = zlib_decode(base64_decode($base64_zlib));

            $html .= "Command line output: ". $cli_output;
            $html .= "Scan duration: " . $root->scandata[0]->children()[0]['duration'] . "<br>";
            $html .= "Total checks: " . $root->scandata[0]->children()[0]['total_checks'] . "<br>";

            //Checking if SQLMap has found an SQL Injection by looking at vulns to see if it's yes
            if ($root->scandata[0]->results[0]->children()[0]['data'] == "yes") {
                // Accessing vuln_parameters
                $vuln_output = $root->scandata[0]->results[0]->children()[1]['data'];
                // Accessing banner
                $banner = $root->scandata[0]->results[0]->children()[2]['data'];
                $html .= "SQL Injection found" . $vuln_output ;
                $html .= "Banner " . $banner ;
            }
            else {
                $html .= "No vulnerable parameters found";
            }

            return View::make('backend/scans/sqlmap/html', array('title' => 'SQLMap scan results', 'html_results' => $html));
        }
        return View::make('backend/scans/unauthorised', ['title' => 'Unauthorised access']);
    }

	public function downloadXml($id) {

        $sqlmap_scan_xml = User::find(Auth::id())->sqlmap_scans()->where('id', '=', $id)->first();
		if ($sqlmap_scan_xml !== null) {
            return (Response::download($sqlmap_scan_xml->results, 'sqlmap_results_' . Auth::id() . '_' . $id . '.xml'));
        }
        return Redirect::back();
    }

}
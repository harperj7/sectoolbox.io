<?php

use Acme\Forms\RegistrationForm;
use Acme\Forms\ChangePasswordForm;

class RegistrationController extends \BaseController {

	function __construct(RegistrationForm $registrationForm, ChangePasswordForm $changePasswordForm) {

		$this->registrationForm = $registrationForm;
        $this->changePasswordForm = $changePasswordForm;

	}

	/**
	 * Display a listing of the resource.
	 * GET /registration
	 *
	 * @return Response
	 */
	public function index()
	{
		//
	}

	/**
	 * Show the form for creating a new resource.
	 * GET /registration/create
	 *
	 * @return Response
	 */
	public function create()
	{

		return View::make('frontend.registration.create2', array('title' => 'Register'));
	}

	/**
	 * Store a newly created resource in storage.
	 * POST /registration
	 *
	 * @return Response
	 */
	public function store()
	{

		$input = Input::only('email', 'password', 'password_confirmation');
		$name = Input::get('name');
		$plan = Input::get('plan');
		$token = Input::get('stripeToken');

		$this->registrationForm->validate($input);

		$user = User::create($input);

		Auth::login($user);

        $user = Auth::user();

		$user->subscription($plan)->create($token, [
				'email' => Auth::user()->email,
				'metadata' => [
					'name_on_card' => $name
				]
		]);

        Event::fire('UserHasSubscribed', array($user));

		return Redirect::route('console');
	}

	/**
	 * Display the specified resource.
	 * GET /registration/{id}
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function show($id)
	{
		//
	}

	/**
	 * Show the form for editing the specified resource.
	 * GET /registration/{id}/edit
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function edit($id)
	{
		return View::make('registration.edit');
	}

	/**
	 * Update the specified resource in storage.
	 * PUT /registration/{id}
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function update($id)
	{
        //
	}

    public function updatePassword() {

        $input = Input::all();
        $this->changePasswordForm->validate($input);

        $user = Auth::user();
        $user->password = $input['password'];

        $user->save();

        return Redirect::back()->with('message', 'Password successfully updated');

    }

	/**
	 * Remove the specified resource from storage.
	 * DELETE /registration/{id}
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function destroy($id)
	{
		//
	}



}

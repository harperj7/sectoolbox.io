<?php

class PagesController extends \BaseController {

	public function showIndex() {

		return View::make('frontend/index');
	}

	public function showPricing() {

		return View::make('frontend/pricing', array('title' => 'Pricing'));
	}

	public function showConsole() {
//		$user = Auth::user();
//		$invoices = $user->subscription()->invoices();
//		// dd($invoices);in_14mO2o4IjBVKR2o2ZOxp5wiE
//		return $user->downloadInvoice('in_14mO2o4IjBVKR2o2ZOxp5wiE', [
//    'vendor'  => 'Security Toolbox',
//    'product' => 'Security scanning tools',
//]);
		 return View::make('backend/index', array('title' => 'Dashboard'));
	}

    public function showUserScans() {

        $time_taken = [];
        $scans = [];

        $scans['nikto'] = Nikto::where('user_id', '=', Auth::id())->orderBy('created_at', 'desc')->take(5)->get();
        $time_taken['nikto'] = time_difference($scans['nikto']);

        $scans['nmap'] = Nmap::where('user_id', '=', Auth::id())->orderBy('created_at', 'desc')->take(5)->get();
        $time_taken['nmap'] = time_difference($scans['nmap']);

        $scans['openvas'] = Openvas::where('user_id', '=', Auth::id())->orderBy('created_at', 'desc')->take(5)->get();
        $time_taken['openvas'] = time_difference($scans['openvas']);

        $scans['sqlmap'] = Sqlmap::where('user_id', '=', Auth::id())->orderBy('created_at', 'desc')->take(5)->get();
        $time_taken['sqlmap'] = time_difference($scans['sqlmap']);

        return View::make('backend/scans', array('title' => 'My recent scans'))->with(compact('scans', 'time_taken'));

    }

    public function showChangePassword() {
        return View::make('backend.user.change-password', array('title' => 'Change password'));
    }

    public function showTerms() {
        return View::make('frontend.terms', array('title' => 'Terms and Conditions'));
    }
}

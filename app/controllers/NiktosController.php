<?php

use Acme\Forms\NiktoForm;

class NiktosController extends \BaseController {

    function __construct(NiktoForm $niktoForm) {

        $this->niktoForm = $niktoForm;

    }

	/**
	 * Display a listing of the resource.
	 * GET /niktos
	 *
	 * @return Response
	 */
	public function index()
	{
		$nikto_scans = Nikto::where('user_id', '=', Auth::id())->paginate(15);

		$time_taken = time_difference($nikto_scans);

		return View::make('backend/scans/nikto/index', array('title' => 'Nikto scans'))->with(compact('nikto_scans', 'time_taken'));

	}

	/**
	 * Show the form for creating a new resource.
	 * GET /niktos/create
	 *
	 * @return Response
	 */
	public function create()
	{

		return View::make('backend/scans/nikto/create', array('title' => 'Create a Nikto scan'));
	
	}

	/**
	 * Store a newly created resource in storage.
	 * POST /niktos
	 *
	 * @return Response
	 */
	public function store()
	{
        $input = Input::all();

        $this->niktoForm->validate($input);
		
		$nikto = new Nikto;
		$previous_id = $nikto->orderBy('id', 'DESC')->pluck('id');
        $previous_id = ($previous_id !== null) ? ($previous_id + 1) : 1;

        $port = (empty($input['port'])) ? '' : $input['port'];

        $target = strip_tags($input['target']);

        $data = [
            'email_result' => 0,
            'IndexID' => $previous_id,
            'UserID' => Auth::id(),
            'email' => Auth::user()->email,
            'scantype' => 'nikto',
            'target' => $target,
            'port' => $port
        ];

		$worker = new Workers;
		$connection = $worker->createConnection();
		$channel 	= $worker->createChannel($connection);
		$queue 		= $worker->createQueue($channel, 'web_queue');
		$message  	= $worker->sendMessage($channel, 'web_queue', $data);
		$closed 	= $worker->closeConnection($connection);

        $nikto->message_sent = json_encode($data);
		$nikto->port = ($port !== '') ? $port : '80, 443';
		$nikto->user_id = Auth::id();
		$nikto->target = $target;
		$nikto->completed = 0;

		$nikto->save();

		return Redirect::back()->with('message', 'Nikto scan successfully created');
	}

	/**
	 * Display the specified resource.
	 * GET /niktos/{id}
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function show($id)
	{
		//
	}

	/**
	 * Show the form for editing the specified resource.
	 * GET /niktos/{id}/edit
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function edit($id)
	{
		//
	}

	/**
	 * Update the specified resource in storage.
	 * PUT /niktos/{id}
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function update($id)
	{
		//
	}

	/**
	 * Remove the specified resource from storage.
	 * DELETE /niktos/{id}
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function destroy($id)
	{
        $nikto_scan = User::find(Auth::id())->nikto_scans()->where('id', '=', $id)->first();

        if ($nikto_scan !== null) {
            $nikto_scan->delete();
            return Redirect::back()->with('niktomessage', 'Nikto scan successfully deleted');
        }
        return Redirect::back();
	}

	public function downloadXml($id) {

        $nikto_scan_xml = User::find(Auth::id())->nikto_scans()->where('id', '=', $id)->first();

        if ($nikto_scan_xml !== null) {
            return (Response::download($nikto_scan_xml->results));
        }
        return Redirect::back();
	}

    public function viewHtml($id) {

        $nikto_scan_xml = User::find(Auth::id())->nikto_scans()->where('id', '=', $id)->first();

        if ($nikto_scan_xml !== null) {

            $html = '';

            $root = simplexml_load_file($nikto_scan_xml->results);
            /* Scan Header information */
            $nikto_options = $root['options'];
            $version = $root['version'];
            $scanstart = $root['scanstart'];
            $scanend =  $root['scanend'];
            $html .= "<html><title>Nikto scan results</title><body>";
            $html .= "Nikto options : " . $nikto_options . "<br>";
            $html .= "Scan Start : " . $scanstart . "<br>";
            $html .= "Scan End : " . $scanend . "<br>";
            $html .= "Scan Details" . "<br>";
            $html .= "Target IP :" . $root->scandetails[0]['targetip'] . "<br>";
            $html .= "targethostname :" . $root->scandetails[0]['targethostname'] . "<br>";
            $html .= "Target Port : " . $root->scandetails[0]['targetport'] . "<br>";
            $html .= "Target Banner : " . $root->scandetails[0]['targetbanner'] . "<br>";
            $html .= "Site Name : " . $root->scandetails[0]['sitename'] . "<br>";
            $html .= "Site IP : " . $root->scandetails[0]['siteip'] . "<br>";
            $html .= "Host Header : " . $root->scandetails[0]['hostheader'] . "<br>";
            $html .= "Errors : " . $root->scandetails[0]['errors'] . "<br>";
            $html .= "Checks : " . $root->scandetails[0]['checks'] . "<br>";

            /* Getting scan Information*/

            foreach ($root->scandetails[0]->item as $data) {
                /*Checking for failed to parse stuff*/
                if ($data->description != "#TEMPL_MSG#"){

                    $html .= "HTTP Method : " .$data['method'] . "<br>";
                    $html .= "URI : " . $data->uri . "<br>";
                    $html .= "Description : " . $data->description . "<br>";
                    $html .= "Test Links : " . $data->namelink . "<br>";
                    $html .= "IPlink : " . $data->iplink . "<br>";
                    $html .= "OSVDB ID : " . $data['osvdbid'] . "<br>";
                    $html .= "OSVDB URL : " . $data['osvdblink'] . "<br>";
                }
            }
            /* Scan footer information */
            $html .= "Elapsed Scan time : " . $root->scandetails[0]->statistics[0]['elapsed'] . "<br>";
            $html .= "Total items found : " . $root->scandetails[0]->statistics[0]['itemsfound'] . "<br>";
            $html .= "Total items tested : " . $root->scandetails[0]->statistics[0]['itemstested'] . "<br>";
            $html .= "End time : " . $root->scandetails[0]->statistics[0]['endtime'] . "<br>";
            $html .= "</body></html>";

            return View::make('backend/scans/nikto/html', array('title' => 'Nikto scan results', 'html_results' => $html));
        }
        return View::make('backend/scans/unauthorised', ['title' => 'Unauthorised access']);
    }

}
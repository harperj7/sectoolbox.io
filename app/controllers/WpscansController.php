<?php

use Acme\Forms\WpscansForm;

class WpscansController extends \BaseController {

	function __construct(WpscansForm $wpscansForm) {

		$this->wpscansForm = $wpscansForm;

	}

	/**
	 * Display a listing of the resource.
	 * GET /wpscans
	 *
	 * @return Response
	 */
	public function index()
	{
		$wpscan_scans = Wpscan::where('user_id', '=', Auth::id())->paginate(15);

		$time_taken = time_difference($wpscan_scans);

		return View::make('backend/scans/wpscan/index', array('title' => 'WPScan scans'))->with(compact('wpscan_scans', 'time_taken'));
	}

	/**
	 * Show the form for creating a new resource.
	 * GET /wpscans/create
	 *
	 * @return Response
	 */
	public function create()
	{
		return View::make('backend/scans/wpscan/create', array('title' => 'Create a WPScan scan'));
	}

	/**
	 * Store a newly created resource in storage.
	 * POST /wpscans
	 *
	 * @return Response
	 */
	public function store()
	{
        $input = Input::all();

		$this->wpscansForm->validate($input);

		$target = $input['target'];
		
		$wpscan = new Wpscan;
        $previous_id = $wpscan->orderBy('id', 'DESC')->pluck('id');
        $previous_id = ($previous_id !== null) ? ($previous_id + 1) : 1;

        $data = [
            'target' => $input['protocol'] . '://' . $input['target'],
            'email_result' => $input['email-me'],
            'IndexID' => $previous_id,
            'UserID' => Auth::id(),
            'email' => Auth::user()->email,
            'threads' => 1,
            'scantype' => 'wpscan'
        ];

		$worker = new Workers;
		$connection = $worker->createConnection();
		$channel 	= $worker->createChannel($connection);
		$queue 		= $worker->createQueue($channel, 'web_queue');
		$message  	= $worker->sendMessage($channel, 'web_queue', $data);
		$closed 	= $worker->closeConnection($connection);

		$wpscan->user_id = Auth::id();
		$wpscan->target = $target;
		$wpscan->completed = 0;

		$wpscan->save();

		return Redirect::back()->with('message', 'WPScan scan successfully created');
	}

	/**
	 * Display the specified resource.
	 * GET /wpscans/{id}
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function show($id)
	{
		//
	}

	/**
	 * Show the form for editing the specified resource.
	 * GET /wpscans/{id}/edit
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function edit($id)
	{
		//
	}

	/**
	 * Update the specified resource in storage.
	 * PUT /wpscans/{id}
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function update($id)
	{
		//
	}

	/**
	 * Remove the specified resource from storage.
	 * DELETE /wpscans/{id}
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function destroy($id)
	{
		$wpscan_scan = Wpscan::find($id);

		$wpscan_scan->delete();

		return Redirect::back()->with('message', 'WPScan scan successfully deleted');
	}

	public function downloadXml($id) {

		$wpscan_scan_xml = Wpscan::find($id);
		
		return (Response::download($wpscan_scan_xml->results));
	}

}
@include('frontend/partials/head')

	<body class="home">

		@include('frontend/partials/navbar')

		<div class="clear-100"></div>
		<!-- container -->
		<div class="container">

			<ol class="breadcrumb">
				<li><a href="/">Home</a></li>
				<li class="active">{{ get_breadcrumb() }}</li>
			</ol>

			<div class="row">

				@yield('content')

			</div>

		</div>	<!-- /container -->

		@include('frontend/partials/footer')

		<!-- JavaScript libs are placed at the end of the document so the pages load faster -->
		<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.10.2/jquery.min.js"></script>
		<script src="https://netdna.bootstrapcdn.com/bootstrap/3.0.0/js/bootstrap.min.js"></script>
		<script src="assets/js/headroom.min.js"></script>
		<script src="assets/js/jQuery.headroom.min.js"></script>
		<script src="assets/js/template.js"></script>
		<script src="https://js.stripe.com/v2/"></script>
		<script type="text/javascript">
		// This identifies your website in the createToken call below
			Stripe.setPublishableKey('pk_test_4LbbbnOfBxBrjl5Amw9zkXfj');
			jQuery(function($) {
				$('#payment-form').submit(function(event) {
					var $form = $(this);

					// Disable the submit button to prevent repeated clicks
					$form.find('button').prop('disabled', true);

					Stripe.card.createToken($form, stripeResponseHandler);

					// Prevent the form from submitting with the default action
					return false;
				});

				function stripeResponseHandler(status, response) {
				  var $form = $('#payment-form');

				  if (response.error) {
				    // Show the errors on the form
				    $form.find('.payment-errors').text(response.error.message);
				    $form.find('button').prop('disabled', false);
				  } else {
				    // response contains id and card, which contains additional card details
				    var token = response.id;
				    // Insert the token into the form so it gets submitted to the server
				    $form.append($('<input type="hidden" name="stripeToken" />').val(token));
				    // and submit
				    $form.get(0).submit();
				  }
				};
			});
		</script>
	</body>
</html>

<!-- Header -->
	<header id="head">
		<div class="container">
			<div class="row">
				<h1 class="lead">HOSTED SECURITY TOOLS</h1>
				<p class="tagline">Security Toolbox: <em>the</em> security scanning toolbox</p>
				<p><a class="btn btn-default btn-lg" role="button">MORE INFO</a> <a href="/login" class="btn btn-action btn-lg" role="button">TRY NOW</a></p>
			</div>
		</div>
	</header>
	<!-- /Header -->
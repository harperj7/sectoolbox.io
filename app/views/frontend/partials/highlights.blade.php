<!-- Highlights - jumbotron -->
	<div class="jumbotron top-space">
		<div class="container">
			
			<h3 class="text-center thin">Reasons to use our service</h3>
			
			<div class="row">
				<div class="col-md-3 col-sm-6 highlight">
					<div class="h-caption"><h4><i class="fa fa-arrows-h fa-5"></i>Monitor changes</h4></div>
					<div class="h-body text-center">
						<p>Monitor your external IT infrastructure for changes with Nmap.</p>
					</div>
				</div>
				<div class="col-md-3 col-sm-6 highlight">
					<div class="h-caption"><h4><i class="fa fa-flash fa-5"></i>Quick scans</h4></div>
					<div class="h-body text-center">
						<p>We configure all our scans for maximum speed and coverage. Scans should not take longer than 20 minutes, giving you results fast.</p>
					</div>
				</div>
				<div class="col-md-3 col-sm-6 highlight">
					<div class="h-caption"><h4><i class="fa fa-shield fa-5"></i>Recognised security tools</h4></div>
					<div class="h-body text-center">
						<p>Using industry standard open source tools (nmap, OpenVAS and Nikto) you can quickly assess your network for vulnerabilities.</p>
					</div>
				</div>
				<div class="col-md-3 col-sm-6 highlight">
					<div class="h-caption"><h4><i class="fa fa-file-text-o fa-5"></i>Reports</h4></div>
					<div class="h-body text-center">
						<p>You can view scan results online. Download reports in either HTML or XML. Have the results emailed to you after the scan is finished.</p>
					</div>
				</div>
			</div> <!-- /row  -->
		
		</div>
	</div>
	<!-- /Highlights -->
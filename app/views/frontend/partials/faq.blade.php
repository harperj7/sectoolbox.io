<!-- container -->
	<div class="container">

		<h2 class="text-center top-space">Frequently Asked Questions</h2>
		<br>

		<div class="row">
			<div class="col-sm-6">
				<h3>What scanning tools are used?</h3>
				<p>Vivamus tincidunt enim augue, sit amet lacinia magna tempor vel. Vivamus vitae dui cursus, euismod sapien eget, consectetur nisi. Proin nec porta nunc.</p>
			</div>
			<div class="col-sm-6">
				<h3>Can I view reports from old scans that have been completed?</h3>
				<p>Yes!</p>
			</div>
		</div> <!-- /row -->

		<div class="row">
			<div class="col-sm-6">
				<h3>Will you email me if there is a change?</h3>
				<p>
					You can set up email alerts for any changes.
				</p>
			</div>
			<div class="col-sm-6">
				<h3>Can I get a refund?</h3>
				<p>We do not offer refunds due to the nature of the service provided.</p>
			</div>
		</div> <!-- /row -->

		<div class="jumbotron top-space">
			<h4>Dicta, nostrum nemo soluta sapiente sit dolor quae voluptas quidem doloribus recusandae facere magni ullam suscipit sunt atque rerum eaque iusto facilis esse nam veniam incidunt officia perspiciatis at voluptatibus. Libero, aliquid illum possimus numquam fuga.</h4>
     		<p class="text-right"><a class="btn btn-primary btn-large">Learn more »</a></p>
  		</div>

	</div>	<!-- /container -->
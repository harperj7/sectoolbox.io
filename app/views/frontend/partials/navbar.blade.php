<!-- Fixed navbar -->
<div class="navbar navbar-inverse navbar-fixed-top headroom" >
	<div class="container">
		<div class="navbar-header">
			<!-- Button for smallest screens -->
			<button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse"><span class="icon-bar"></span> <span class="icon-bar"></span> <span class="icon-bar"></span> </button>
			<a class="navbar-brand" href="{{ url('/', null, null) }}"><img src="{{ asset('assets/images/security-toolbox-logo-s.png') }}" alt="Security Toolbox"></a>
		</div>
		<div class="navbar-collapse collapse">
			<ul class="nav navbar-nav pull-right">
				<li class="{{ set_active('/') }}">
					{{ link_to('/', 'Home', $attributes = array('class' => set_active('/')), $secure = null) }}
				</li>
				<li class="{{ set_active('pricing') }}">
				    <a href="#">Pricing</a>
				</li>
				<li class="{{ set_active('about') }}">
					<a href="#">About</a>
				</li>
				<li class="{{ set_active('faq') }}">
					<a href="#">FAQ</a>
				</li>
				<!-- <li class="dropdown">
					<a href="#" class="dropdown-toggle" data-toggle="dropdown">More Pages <b class="caret"></b></a>
					<ul class="dropdown-menu">
						<li><a href="sidebar-left.html">Left Sidebar</a></li>
						<li class="active"><a href="sidebar-right.html">Right Sidebar</a></li>
					</ul>
				</li> -->
				<li class="{{ set_active('contact') }}">
					<a href="#">Contact</a>
				</li>
				@if (Auth::guest())
					<li class="{{ set_active('login') }}">
						<!-- <a class="btn" href="login">SIGN IN / SIGN UP</a> -->
						{{ link_to('login', 'SIGN IN / SIGN UP', $attributes = array('class' => set_active('login') . ' btn'), $secure = null) }}
					</li>
				@else
					<li>
						{{ link_to('console', 'CONSOLE', $attributes = array('class' => set_active('console') . ' btn'), $secure = null) }}
					</li>
				@endif
			</ul>
		</div><!--/.nav-collapse -->
	</div>
</div> 
<!-- /.navbar -->
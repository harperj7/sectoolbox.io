@extends('frontend/master')

@section('content')
	
	<!-- Article main content -->
	<article class="col-xs-12 maincontent">
		<header class="page-header">
			<h1 class="page-title">Log in</h1>
		</header>

		<div class="col-md-6 col-md-offset-3 col-sm-8 col-sm-offset-2">
			<div class="panel panel-primary">
				<div class="panel-body">

					<h3 class="thin text-center">Log in to your account</h3>
						<p class="text-center text-muted">Log in to your account below.</p>
						<hr>

					{{ Form::open(['route' => 'Sessions.store']) }}

						<div class="top-margin">
							{{ Form::label('email', 'Email:') }}
							{{ Form::text('email', null, ['class' => 'form-control', 'required' => 'required']) }}
							{{ errors_for('email', $errors) }}
						</div>

						<div class="top-margin">
							{{ Form::label('password', 'Password:') }}
							{{ Form::password('password', ['class' => 'form-control', 'required' => 'required']) }}
							{{ errors_for('password', $errors) }}
						</div>

						<div class="top-margin">
							{{ Form::submit('Log in', ['class' => 'btn btn-primary']) }}
						</div>

						@if (Session::has('flash_message'))
							<div class="form-group">
								<div class="alert alert-info" role="alert">{{ Session::get('flash_message') }}</div>
							</div>
						@endif

					{{ Form::close() }}

				</div>
			</div>
		</div>
	</article>
@stop
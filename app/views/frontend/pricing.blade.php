@extends('frontend/master')

@section('content')
	<!-- Article main content -->
	<article class="col-sm-12 maincontent">
		<header class="page-header">
			<h1 class="page-title">Pricing</h1>
		</header>
		<div>&nbsp;</div>
		<div class="col-sm-4 pricing-column">
          <div class="panel panel-default text-center">
            <div class="panel-heading">
              <h3>Basic</h3>
            </div>
            <div class="panel-body">
              <h3 class="panel-title price">&#163;9<span class="price-cents">99</span><span class="price-month">month</span></h3>
            </div>
            <ul class="list-group">
              <li class="list-group-item">5 scans/day</li>
              <li class="list-group-item">Complete Nmap access</li>
              <li class="list-group-item">Stuff</li>
              <li class="list-group-item">Free cake</li>
              <li class="list-group-item">Security Suite</li>
              <!-- <li class="list-group-item"><a class="btn btn-default">Sign up</a></li> -->
							<li class="list-group-item">
							<form action="" method="POST">
							  <script
							    src="https://checkout.stripe.com/checkout.js" class="stripe-button"
							    data-key="pk_test_4LbbbnOfBxBrjl5Amw9zkXfj"
							    data-amount="1000"
							    data-name="Security Toolbox"
							    data-description="Monthly standard subscription ($10.00)"
							    data-image="/128x128.png">
							  </script>
							</form>
							</li>
            </ul>
          </div>
        </div>
        <div class="col-sm-4 pricing-column ott">
          <div class="panel panel-primary text-center pricing-recommended">
            <div class="panel-heading">
              <h3>Plus</h3>
            </div>
            <div class="panel-body">
              <h3 class="panel-title price">&#163;19<span class="price-cents">99</span><span class="price-month">month</span></h3>
            </div>
            <ul class="list-group">
              <li class="list-group-item">25 scan/day</li>
              <li class="list-group-item">Access to 3 security tools</li>
              <li class="list-group-item">More stuff</li>
              <li class="list-group-item">Free really nice cake</li>
              <li class="list-group-item">Security Suite</li>
              <li class="list-group-item"><a class="btn btn-primary">Sign up</a></li>
            </ul>
          </div>
        </div>
        <div class="col-sm-4 pricing-column">
          <div class="panel panel-default text-center">
            <div class="panel-heading">
              <h3>Premium</h3>
            </div>
            <div class="panel-body">
              <h3 class="panel-title price">&#163;29<span class="price-cents">99</span><span class="price-month">month</span></h3>
            </div>
            <ul class="list-group">
              <li class="list-group-item">100 scans/day</li>
              <li class="list-group-item">Access to all security tools</li>
              <li class="list-group-item">Even more stuff</li>
              <li class="list-group-item">Free cake, plus biscuits!</li>
              <li class="list-group-item">Security Suite</li>
              <li class="list-group-item"><a class="btn btn-default">Sign up</a></li>
            </ul>
          </div>
        </div>
	</article>
	<!-- /Article -->
@stop

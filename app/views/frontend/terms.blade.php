@extends('frontend/master')

 @section('content')
 	<!-- Article main content -->
 	<article class="col-sm-12 maincontent">
 		<header class="page-header">
 			<h1 class="page-title">Terms and Conditions</h1>
 		</header>
        <p>If you continue to browse and use this website, you are agreeing to comply with and be bound by the following terms and conditions of use, which together with our privacy policy govern securitytoolbox.io’s relationship with you in relation to this website. If you disagree with any part of these terms and conditions, you must leave this site and not register to use this website.</p>
        <h2>Definitions</h2>
        <p>The term 'securitytoolbox.io' or 'us' or 'we' refers to securitytoolbox.io and its network of servers used to conduct scanning services. The term 'you' refers to the user, viewer or customer of our website. The term ‘host’ refers to any internet connected device that may be accessed by an IPv4, IPv6 or URL by one of our scanners. The term ‘scanning services’ refers to using of security toolbox tools against a host.</p>
        <h3>Use of scanning services</h3>
        <p>You understand and acknowledge that by using securitytoolbox.io scanning services:</p>
        <ul>
            <li>You will only scan hosts that you have explicit permission and authorization to scan.</li>
            <li>You consent to securitytoolbox.io scanning hosts you request.</li>
            <li>You use the website at your own risk and take full responsibility for its usage.</li>
            <li>securitytoolbox.io is NOT liable for any damages that may arise from using the scanning services.</li>
            <li>Once a scan has been started, it cannot be cancelled.</li>
        </ul>
        <h2>Use of this website is subject to the following general terms of use</h2>
        <ul>
            <li>The content of the pages of this website is for your general information and use only. It is subject to change without notice.</li>
            <li>Neither we nor any third parties provide any warranty or guarantee as to the accuracy, timeliness, performance, completeness or suitability of the information and materials found or offered on this website for any particular purpose. You acknowledge that such information and materials may contain inaccuracies or errors and we expressly exclude liability for any such inaccuracies or errors to the fullest extent permitted by law.</li>
            <li>Your use of any information or materials on this website is entirely at your own risk, for which we shall not be liable. It shall be your own responsibility to ensure that any products, services or information available through this website meet your specific requirements.</li>
            <li>This website contains material which is owned by or licensed to us. This material includes, but is not limited to, the design, layout, look, appearance and graphics. Reproduction is prohibited other than in accordance with the copyright notice, which forms part of these terms and conditions.</li>
            <li>All trademarks reproduced in this website, which are not the property of, or licensed to the operator, are acknowledged on the website.</li>
            <li>Unauthorised use of this website may give rise to a claim for damages and/or be a criminal offence.</li>
            <li>From time to time, this website may also include links to other websites. These links are provided for your convenience to provide further information. They do not signify that we endorse the website(s). We have no responsibility for the content of the linked website(s).</li>
            <li>Your use of this website and any dispute arising out of such use of the website is subject to the laws of England, Northern Ireland, Scotland and Wales.</li>
        </ul>
 	</article>
 	<!-- /Article -->
 @stop
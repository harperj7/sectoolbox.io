@extends('frontend/master')

@section('content')

	<!-- Article main content -->
	<article class="col-xs-12 maincontent">
		<header class="page-header">
			<h1 class="page-title">Register</h1>
		</header>

		<div class="col-md-6 col-md-offset-3 col-sm-8 col-sm-offset-2">
			<div class="panel panel-primary">
				<div class="panel-body">

							<h3 class="thin text-center">Create a new account</h3>
							<p class="text-center text-muted">Register a new account below. If you already have an account <a href="http://securitytoolbox.io/login">log in</a>.</p>
							<hr>

					{{ Form::open(['route' => 'registration.store']) }}

						<div class="top-margin">
							{{ Form::label('email', 'Email:') }}
							{{ Form::text('email', null, ['class' => 'form-control', 'required' => 'required']) }}
							{{ errors_for('email', $errors) }}
						</div>

						<div class="row top-margin">
							<div class="col-sm-6">
								{{ Form::label('password', 'Password:') }}
								{{ Form::password('password', ['class' => 'form-control', 'required' => 'required']) }}
								{{ errors_for('password', $errors) }}
							</div>

							<div class="col-sm-6">
								{{ Form::label('password_confirmation', 'Verify password:') }}
								{{ Form::password('password_confirmation', ['class' => 'form-control', 'required' => 'required']) }}
							</div>
						</div>

						<div class="top-margin">
							<noscript>Please enable Javascript to register for an account</noscript>
							<form action="" method="POST">
								<script
									src="https://checkout.stripe.com/checkout.js" class="stripe-button"
									data-key="pk_test_4LbbbnOfBxBrjl5Amw9zkXfj"
									data-amount="1000"
									data-name="Security Toolbox"
									data-description="Monthly standard subscription ($10.00)"
									data-image="/128x128.png">
								</script>
							</form>
						</div>

					{{ Form::close() }}

				</div>

			</div>
		</div>

	</article>
	<!-- /Article -->

@stop

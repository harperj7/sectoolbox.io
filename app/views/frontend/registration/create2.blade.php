@extends('frontend/master')

@section('content')

  <!-- Article main content -->
  <article class="col-xs-12 maincontent">
    <header class="page-header">
      <h1 class="page-title">Register</h1>
    </header>

    <div class="col-md-6 col-md-offset-3 col-sm-8 col-sm-offset-2">
      <div class="panel panel-primary">
        <div class="panel-body">

              <h2 class="thin text-center">Create a new account</h2>
              <p class="text-center text-muted">Register a new account below. If you already have an account <a href="http://securitytoolbox.io/login">log in</a>.</p>
              <hr>

          {{ Form::open(['route' => 'registration.store', 'id' => 'payment-form']) }}
            <h3 class="thin">Account information</h3>
            <hr>
            <div class="top-margin">
              {{ Form::label('email', 'Email:') }}
              {{ Form::text('email', null, ['class' => 'form-control', 'required' => 'required']) }}
              {{ errors_for('email', $errors) }}
            </div>

            <div class="row top-margin">
              <div class="col-sm-6">
                {{ Form::label('password', 'Password:') }}
                {{ Form::password('password', ['class' => 'form-control', 'required' => 'required']) }}
                {{ errors_for('password', $errors) }}
              </div>

              <div class="col-sm-6">
                {{ Form::label('password_confirmation', 'Verify password:') }}
                {{ Form::password('password_confirmation', ['class' => 'form-control', 'required' => 'required']) }}
              </div>
            </div>
            <h3 class="thin top-margin">Billing information</h3>
            <hr>
            <div class="form-row top-margin">
              <label>
                <span>Plan</span>
                <select class="form-control" id="plan" name="plan">
                  <option value="basicmonthly">
                      10 USD every month.
                  </option>
                  <option selected="selected" value="biannual">
                      48 USD every 6 months. Save 20%.
                  </option>
              </select>
              </label>
            </div>

            <div class="row top-margin">
              <div class="col-sm-5">
                <label>
                  <span>Name on card</span>
                  <input type="text" name="name" required="required" class="form-control" />
                </label>
              </div>

              <div class="col-sm-7">
                <label>
                  <span>Card Number</span>
                  <input type="text" size="20" data-stripe="number" required="required" class="form-control" />
                </label>
              </div>
            </div>

            <div class="row top-margin">
            <div class="col-sm-3">
              <label>
                <span>Security code</span>
                <input type="text" size="4" data-stripe="cvc" required="required" class="form-control" />
              </label>
            </div>

            <div class="col-sm-9">
              <label>
                <span>Expiration date</span>
              </label
                <div class="form-group">
                  <div class="col-sm-7">
                <select class="form-control" data-stripe="exp-month" id="cc-expiration-month">
                  <option value="01">
                      January (01)
                  </option>
                  <option value="02">
                      February (02)
                  </option>
                  <option value="03">
                      March (03)
                  </option>
                  <option value="04">
                      April (04)
                  </option>
                  <option value="05">
                      May (05)
                  </option>
                  <option value="06">
                      June (06)
                  </option>
                  <option value="07">
                      July (07)
                  </option>
                  <option value="08">
                      August (08)
                  </option>
                  <option value="09">
                      September (09)
                  </option>
                  <option value="10">
                      October (10)
                  </option>
                  <option value="11">
                      November (11)
                  </option>
                  <option value="12">
                      December (12)
                  </option>
              </select>
            </div>
              <div class="col-sm-5">
              <select class="form-control" data-stripe="exp-year" id="cc-expiration-year">
                <option value="2014">
                    2014
                </option>
                <option value="2015">
                    2015
                </option>
                <option value="2016">
                    2016
                </option>
                <option value="2017">
                    2017
                </option>
                <option value="2018">
                    2018
                </option>
                <option value="2019">
                    2019
                </option>
                <option value="2020">
                    2020
                </option>
                <option value="2021">
                    2021
                </option>
                <option value="2022">
                    2022
                </option>
                <option value="2023">
                    2023
                </option>
                <option value="2024">
                    2024
                </option>
                <option value="2025">
                    2025
                </option>
                <option value="2026">
                    2026
                </option>
                <option value="2027">
                    2027
                </option>
                <option value="2028">
                    2028
                </option>
                <option value="2029">
                    2029
                </option>
                <option value="2030">
                    2030
                </option>
            </select>
          </div>
          </div>
            </div>
          </div>

            <div class="form-row top-margin">
              <img src="assets/images/amex-card.png" alt="American Express" />
              <img src="assets/images/mastercard.png" alt="MasterCard" />
              <img src="assets/images/visa-card.png" alt="Visa" />
              <img src="assets/images/stripe.png" alt="Powered by Stripe" />
            </div>

            <div class="top-margin">
              <button type="submit" class="btn btn-primary">Confirm</button>
            </div>

          {{ Form::close() }}

        </div>

      </div>
    </div>

  </article>
  <!-- /Article -->

@stop

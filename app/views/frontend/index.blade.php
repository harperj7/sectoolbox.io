@include('frontend/partials/head')

	<body class="home">

		@include('frontend/partials/navbar')

		@include('frontend/partials/header')

		@include('frontend/partials/intro')
			
		@include('frontend/partials/highlights')

		@include('frontend/partials/faq')
		
		@include('frontend/partials/footer')
			
		<!-- JavaScript libs are placed at the end of the document so the pages load faster -->
		<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.10.2/jquery.min.js"></script>
		<script src="https://netdna.bootstrapcdn.com/bootstrap/3.0.0/js/bootstrap.min.js"></script>
		<script src="assets/js/headroom.min.js"></script>
		<script src="assets/js/jQuery.headroom.min.js"></script>
		<script src="assets/js/template.js"></script>
	</body>
</html>
<!-- Navigation -->
<nav class="navbar navbar-default navbar-static-top" role="navigation" style="margin-bottom: 0">
    <div class="navbar-header">
        <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
            <span class="sr-only">Toggle navigation</span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
        </button>
        <a class="navbar-brand" href="{{ url('/', null, null) }}"><img src="{{ asset('assets/images/security-toolbox-logo-es.png') }}" alt="Security Toolbox"></a>
    </div>
    <!-- /.navbar-header -->

    <ul class="nav navbar-top-links navbar-right">
        
        <!-- /.dropdown -->
        {{--<li class="dropdown">--}}
            {{--<a class="dropdown-toggle" data-toggle="dropdown" href="#">--}}
                {{--<i class="fa fa-bell fa-fw"></i>  <i class="fa fa-caret-down"></i>--}}
            {{--</a>--}}
            {{--<ul class="dropdown-menu dropdown-alerts">--}}
                {{--<li>--}}
                    {{--<a href="#">--}}
                        {{--<div>--}}
                            {{--<i class="fa fa-arrows-h fa-fw"></i> Port change--}}
                            {{--<span class="pull-right text-muted small">55 minutes ago</span>--}}
                        {{--</div>--}}
                    {{--</a>--}}
                {{--</li>--}}
                {{--<li class="divider"></li>--}}
                {{--<li>--}}
                    {{--<a href="#">--}}
                        {{--<div>--}}
                            {{--<i class="fa fa-check-square-o fa-fw"></i> WPScan completed--}}
                            {{--<span class="pull-right text-muted small">2 minutes ago</span>--}}
                        {{--</div>--}}
                    {{--</a>--}}
                {{--</li>--}}
                {{--<li class="divider"></li>--}}
                {{--<li>--}}
                    {{--<a href="#">--}}
                        {{--<div>--}}
                            {{--<i class="fa fa-check-square-o fa-fw"></i> Nikto scan completed--}}
                            {{--<span class="pull-right text-muted small">4 minutes ago</span>--}}
                        {{--</div>--}}
                    {{--</a>--}}
                {{--</li>--}}
                {{--<li class="divider"></li>--}}
                {{--<li>--}}
                    {{--<a class="text-center" href="#">--}}
                        {{--<strong>See all alerts</strong>--}}
                        {{--<i class="fa fa-angle-right"></i>--}}
                    {{--</a>--}}
                {{--</li>--}}
            {{--</ul>--}}
            {{--<!-- /.dropdown-alerts -->--}}
        {{--</li>--}}
        <!-- /.dropdown -->
        <li class="dropdown">
            <a class="dropdown-toggle" data-toggle="dropdown" href="#">
                <i class="fa fa-user fa-fw"></i>  <i class="fa fa-caret-down"></i>
            </a>
            <ul class="dropdown-menu dropdown-user">
                <li><a href="{{ url('console/change-password', null, true) }}"><i class="fa fa-user fa-fw"></i> Change password</a>
                </li>
                <li>
                <!-- <a href="{{ url('/', array('console', 'logout'), true) }}"><i class="fa fa-sign-out fa-fw"></i> Log out</a> -->
                {{ Form::open(array('action' => 'SessionsController@destroy', 'method' => 'delete')) }}
                   <button class="btn menu-btn" type="submit">Log out</button>
                {{ Form::close() }}
                </li>
            </ul>
            <!-- /.dropdown-user -->
        </li>
        <!-- /.dropdown -->
    </ul>
    <!-- /.navbar-top-links -->

    <div class="navbar-default sidebar" role="navigation">
        <div class="sidebar-nav navbar-collapse">
            <ul class="nav" id="side-menu">
                <li class="sidebar-search">
                    <div class="input-group custom-search-form" data-toggle='tooltip' data-placement="right" title='Search is currently disabled; it will be added soon.'>
                        <input type="text" class="form-control" placeholder="Search...">
                        <span class="input-group-btn">
                        <button class="btn btn-default" type="button" disabled="disabled">
                            <i class="fa fa-search"></i>
                        </button>
                    </span>
                    </div>
                    <!-- /input-group -->
                </li>
                <li>
                    <a href="{{ url('console', null, null) }}" title="Dashboard" class="{{ set_active('console') }}">
                    <i class="fa fa-dashboard fa-fw"></i> Dashboard</a>
                </li>
                <li>
                    <a href="{{ url('console/scans', null, null) }}" title="My scans"><i class="fa fa-cogs fa-fw"></i> My scans</a>
                </li>
                <li>
                    <a href="#"><i class="fa fa-bar-chart-o fa-fw"></i> Nikto<span class="fa arrow"></span></a>
                    <ul class="nav nav-second-level">
                        <li>
                            <!-- <a class="{{ set_active('console/nikto/create') }}" href="http://localhost/localsecuritytoolbox.io/public/console/nikto/create">Create a Nikto scan</a> -->
                            {{ link_to('console/nikto/create', 'Create a Nikto scan', $attributes = array('class' => set_active('console/nikto/create')), $secure = null) }}
                        </li>
                        <li>
                            <!-- <a href="/console/nikto">View Nikto scans</a> -->
                            {{ link_to('console/nikto', 'View Nikto scans', $attributes = array('class' => set_active('console/nikto')), $secure = null) }}
                        </li>
                    </ul>
                    <!-- /.nav-second-level -->
                </li>
                <li>
                    <a href="#"><i class="fa fa-bar-chart-o fa-fw"></i> Nmap<span class="fa arrow"></span></a>
                    <ul class="nav nav-second-level">
                        <li>
                            <!-- <a class="{{ set_active('console/nmap/create') }}" href="">Create an Nmap scan</a> -->
                            {{ link_to('console/nmap/create', 'Create an Nmap scan', $attributes = array('class' => set_active('console/nmap/create')), $secure = null) }}
                        </li>
                        <li>
                            <!-- <a href="/console/nmap">View Nmap scans</a> -->
                            {{ link_to('console/nmap', 'View Nmap scans', $attributes = array('class' => set_active('console/nmap')), $secure = null) }}
                        </li>
                    </ul>
                    <!-- /.nav-second-level -->
                </li>
                <li>
                    <a href="#"><i class="fa fa-bar-chart-o fa-fw"></i> OpenVAS<span class="fa arrow"></span></a>
                    <ul class="nav nav-second-level">
                        <li>
                            <!-- <a class="{{ set_active('console/openvas/create') }}" href="http://localhost/localsecuritytoolbox.io/public/console/openvas/create">Create an OpenVAS scan</a> -->
                            {{ link_to('console/openvas/create', 'Create an OpenVAS scan', $attributes = array('class' => set_active('console/openvas/create')), $secure = null) }}
                        </li>
                        <li>
                            <!-- <a href="/console/openvas">View OpenVAS scans</a> -->
                            {{ link_to('console/openvas', 'View OpenVAS scans', $attributes = array('class' => set_active('console/openvas')), $secure = null) }}
                        </li>
                    </ul>
                    <!-- /.nav-second-level -->
                </li>
                <li>
                    <a href="#"><i class="fa fa-bar-chart-o fa-fw"></i> SQLMap<span class="fa arrow"></span></a>
                    <ul class="nav nav-second-level">
                        <li>
                            <!-- <a class="{{ set_active('console/sqlmap/create') }}" href="http://localhost/localsecuritytoolbox.io/public/console/sqlmap/create">Create an sqlmap Scan</a> -->
                            {{ link_to('console/sqlmap/create', 'Create an SQLMap scan', $attributes = array('class' => set_active('console/sqlmap/create')), $secure = null) }}
                        </li>
                        <li>
                            <!-- <a href="/console/sqlmap">View sqlmap scans</a> -->
                            {{ link_to('console/sqlmap', 'View SQLMap scans', $attributes = array('class' => set_active('console/sqlmap')), $secure = null) }}
                        </li>
                    </ul>
                    <!-- /.nav-second-level -->
                </li>

            </ul>
        </div>
        <!-- /.sidebar-collapse -->
    </div>
    <!-- /.navbar-static-side -->
</nav>
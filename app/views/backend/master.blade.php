<!DOCTYPE html>
<html lang="en">

<head>

    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">

    <title>{{ isset($title) ? $title . ' : Security Toolbox Console' : 'Security Toolbox Console' }}</title>

    <!-- Bootstrap Core CSS -->
    <link href="{{ asset('assets/css/bootstrap3.min.css') }}" rel="stylesheet">

    <!-- MetisMenu CSS -->
    <link href="{{ asset('assets/css/plugins/metisMenu/metisMenu.min.css') }}" rel="stylesheet">
    <link href="{{ asset('assets/css/bootstrap-switch.min.css') }}" rel="stylesheet">

    <!-- Custom CSS -->
    <link href="{{ asset('assets/css/sb-admin-2.css') }}" rel="stylesheet">

    <!-- Custom Fonts -->
    <link href="{{ asset('assets/font-awesome-4.1.0/css/font-awesome.min.css') }}" rel="stylesheet" type="text/css">

    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
        <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
        <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
    <![endif]-->
<style>.hidden {display:none !important; }</style>
</head>

<body>

    <div id="wrapper">

        @include('backend/partials/navigation')

        <div id="page-wrapper">
            <div class="row">
                <div class="col-lg-12">
                    @yield('content')
                </div>
                <!-- /.col-lg-12 -->
            </div>
            <!-- /.row -->
        </div>
        <!-- /#page-wrapper -->

    </div>
    <!-- /#wrapper -->

    <!-- jQuery Version 1.11.0 -->
    <script src="{{ asset('assets/js/jquery-1.11.0.js') }}"></script>

    <!-- Bootstrap Core JavaScript -->
    <script src="{{ asset('assets/js/bootstrap.min.js') }}"></script>

    <!-- Metis Menu Plugin JavaScript -->
    <script src="{{ asset('assets/js/plugins/metisMenu/metisMenu.min.js') }}"></script>

    <!-- Custom Theme JavaScript -->
    <script src="{{ asset('assets/js/sb-admin-2.js') }}"></script>

    <script src="{{ asset('assets/js/bootstrap-switch.min.js') }}"></script>

    <script>
    // $('#myTab a').click(function (e) {
    //   e.preventDefault()
    //   $(this).tab('show')
    // })

    $(function () { $("[data-toggle='tooltip']").tooltip(); });
    $('#scan_type').on('change', function() {
        var scan_type = $('#scan_type option:selected').val();
        $('.tcp-only').each(function(){
            $(this).toggleClass('hidden');
        });
        if (scan_type === 'nmap_udp') {
            $('#technique').addClass('hidden');
        } else {
            $('#technique').removeClass('hidden');
        }
    });

    (function(){
        $('#ports').on('change', function(){
            var port_option = $('#ports option:selected').val();
            if (port_option == 'port_range' && $('#port-range').hasClass('hidden') == true) {
                $('#port-range').toggleClass('hidden');
            } else {
                $('#port-range').addClass('hidden');
            }
            if (port_option == 'port_list' && $('#port-list').hasClass('hidden') == true) {
                $('#port-list').toggleClass('hidden');
            } else {
                $('#port-list').addClass('hidden');
            }
        });

        $('#sslports').on('change', function(){
            var port_option = $('#sslports option:selected').val();
            if (port_option == 'port_range' && $('#sslport-range').hasClass('hidden') == true) {
                $('#sslport-range').toggleClass('hidden');
            } else {
                $('#sslport-range').addClass('hidden');
            }
            if (port_option == 'port_list' && $('#sslport-list').hasClass('hidden') == true) {
                $('#sslport-list').toggleClass('hidden');
            } else {
                $('#sslport-list').addClass('hidden');
            }
        });
        $('.btn').button();
        $(".switch").bootstrapSwitch();
        $('.switch').on('switchChange.bootstrapSwitch', function(event, state) {

            if ($(this).attr('value') == 0) {
                $(this).attr('value', '1');
            } else {
                $(this).attr('value', '0');
            }

        });

    })();

    </script>

</body>

</html>
@extends('backend/master')

@section('content')

	<div class="panel panel-primary col-sm-6" style="margin-top:20px;">
		<div class="panel-body">

			<h3 class="thin text-center">Create a new OpenVAS scan</h3>
			<hr>
			<div class="col-sm-12">
			@if(Session::has('message'))
                <div class="alert alert-success" role="alert">
                    <p>{{ Session::get('message') }}</p>
                </div><br />
            @endif
			{{ Form::open(['route' => 'console.openvas.store', 'class' => 'form-horizontal', 'role' => 'form']) }}

				<div class="form-group">
					{{ Form::label('target', 'Target IPv4/IPv6 address', ['class' => 'control-label col-xs-4']) }}
					<div class="col-xs-8">
					    {{ Form::text('target', null, ['class' => 'form-control', 'required' => 'required']) }}
					</div>
				</div>
				{{ errors_for('target', $errors) }}
				<div class="form-group">
                    {{ Form::label('email-me', 'Email me the results', ['class' => 'control-label col-xs-4']) }}
                    <div class="col-xs-8">
                        <div class="btn-group" data-toggle="buttons">
                            <label class="btn btn-toggle active">
                                <input type="radio" name="email-me" id="email-me" checked value="0"> No
                            </label>
                            <label class="btn btn-toggle" data-toggle="tooltip" title="Feature coming soon">
                                <input type="radio" name="email-me" value="1" disabled="disabled"> Yes
                            </label>
                        </div>
                    </div>
                </div>
				<div class="form-group">
                    <div class="col-xs-offset-4 col-xs-8">
                        <div class="checkbox">
                            <label><input type="checkbox" name="terms" value="1" checked="checked" required="required"> I understand and agree to the <a href="{{ url('terms-and-conditions', null, true) }}" title="Terms and Conditions">Terms &amp; Conditions</a></label>
                        </div>
                    </div>
                </div>
                <div class="form-group ">
                    <div class="col-xs-offset-4 col-xs-8">
                        {{ Form::submit('Create scan', ['class' => 'btn btn-primary']) }}
                    </div>
                </div>

			{{ Form::close() }}
			</div>
		</div>
	</div>

	<!-- Start second panel -->
    <div class="panel panel-primary col-sm-6" style="margin-top:20px;">
        <div class="panel-body">
            <h3 class="thin text-center">About the OpenVAS scan</h3>
            <hr>
            <p>OpenVas is a vulnerability scanner that scans for known vulnerabilities. It’s one of the most intrusive scans because it will run many checks against the target. We have configured it to use only safe checks which means it won’t run unsafe checks that might harm your system. Scans are also configured so that it does not rely on ICMP being enabled to scan the host as it will use the presence of services to determine if the host is alive.</p>
            <p>OpenVas also gives you a remediation in the report</p>
            <h4>Target</h4>
            <p>OpenVas only needs the IPv4 or IPv6 address of the host you are scanning.</p>
            <h4>Scan time</h4>
            <p>Scans should take around 20 minutes but if your server is slow it may take longer.</p>
        </div>
    </div>

@stop
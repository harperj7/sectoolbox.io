@extends('backend/master')

@section('content')

	<div class="panel panel-primary col-sm-6" style="margin-top:20px;">
		<div class="panel-body">

			<h3 class="thin text-center">Create a new Nmap scan</h3>
			<hr>

			<!-- Nav tabs -->
            <ul class="nav nav-tabs" role="tablist">
              <li class="active"><a href="#default" role="tab" data-toggle="tab">UDP/TCP scan</a></li>
              <li><a href="#ssl" role="tab" data-toggle="tab">SSL check</a></li>
            </ul>
							
			<div class="tab-content">
			
				<div class="tab-pane fade in active" id="default">

					<div class="col-sm-12">
					<br />
					@if(Session::has('message'))
                        <div class="alert alert-success" role="alert">
                            <p>{{ Session::get('message') }}</p>
                        </div><br />
                    @endif
					{{ Form::open(['route' => 'console.nmap.store', 'class' => 'form-horizontal', 'role' => 'form']) }}
                        <br/>
                        <div class="form-group">
                            {{ Form::label('scan_type', 'Scan type', ['class' => 'control-label col-xs-4']) }}
                            <div class="col-xs-4">
                                {{ Form::select('scan_type', array('nmap_tcp' => 'TCP', 'nmap_udp' => 'UDP'), null, ['class' => 'form-control']) }}
                            </div>
                        </div>
                        {{ errors_for('scan_type', $errors) }}
						<div class="form-group">
							{{ Form::label('target', 'Target', ['class' => 'control-label col-xs-4']) }}
							<div class="col-xs-8">
							    {{ Form::text('target', null, ['class' => 'form-control', 'data-toggle' => 'tooltip', 'title' => 'IPv4, IPv6 or hostname']) }}
							</div>
						</div>
                        {{ errors_for('target', $errors) }}
						<div class="form-group" id="technique">
                            {{ Form::label('port_scan_technique', 'Port scan technique', ['class' => 'control-label col-xs-4']) }}
                            <div class="col-xs-6">
                                <select id="port_scan_technique" name="port_scan_technique" class="form-control">
                                    <option value="synscan">SYN scan (-sS)</option>
                                    <option value="tcpconnectscan">TCP connection scan (-sT)</option>
                                </select>
                            </div>
                        </div>
                        {{ errors_for('port_scan_technique', $errors) }}
                        <hr/>
						<div class="form-group">
							{{ Form::label('ports', 'Ports to scan', ['class' => 'control-label col-xs-4']) }}
							<div class="col-xs-8">
                                <select id="ports" name="ports" class="form-control">
                                    <option value="top_100" class="">Top 100 ports</option>
                                    <option value="top_1000" class="tcp-only">Top 1000 ports</option>
                                    <option value="top_2500" class="tcp-only">Top 2500 ports</option>
                                    <option value="all_ports" class="tcp-only">All ports</option>
                                    <option value="port_list" class="">Comma separated list of ports</option>
                                    <option value="port_range" class="">Custom range of ports</option>
                                </select>
                            </div>
						</div>
						{{ errors_for('ports', $errors) }}
						<div id="port-range" class="hidden">
                            <div class="form-group">
                                <label for="start_port" class="col-xs-4 control-label">Starting port</label>
                                <div class="col-xs-4">
                                    {{ Form::text('start_port', null,['class' => 'form-control', 'data-toggle' => 'tooltip', 'title' => 'Start port']) }}
                                </div>
                            </div>
                            <div class="form-group">
                                <label for="end_port" class="col-xs-4 control-label">Ending port</label>
                                <div class="col-xs-4">
                                    {{ Form::text('end_port', null,['class' => 'form-control', 'data-toggle' => 'tooltip', 'title' => 'End port']) }}
                                </div>
                            </div>
						</div>
						<div id="port-list" class="form-group hidden">
						    <label for="port_comma_list" class="col-xs-4 control-label">Scan the following port list</label>
						    <div class="col-sm-8">
                                <textarea name="port_comma_list" class="form-control" data-toggle='tooltip' title='Comma separated list of ports'></textarea>
                            </div>
                        </div>
                        <div class="form-group">
                            {{ Form::label('ports', 'Services / default scripts', ['class' => 'control-label col-xs-4']) }}
                            <div class="col-xs-8">
                                <select id="services_scripts" name="services_scripts" class="form-control">
                                    <option value="none">No versions or scripts</option>
                                    <option value="version_only">Enumerate services' versions</option>
                                    <option value="version_scripts">Run Nmap scripts & enumerate versions</option>
                                </select>
                            </div>
                        </div>
                        {{ errors_for('services_scripts', $errors) }}
						<div class="form-group">
							{{ Form::label('ping', 'Assume the target is up? (-Pn)', ['class' => 'control-label col-xs-4']) }}
							<div class="col-xs-8">
                                <div class="btn-group" data-toggle="buttons">
                                    <label class="btn btn-toggle active">
                                        <input type="radio" name="ping" id="ping" checked value="0"> No
                                    </label>
                                    <label class="btn btn-toggle">
                                        <input type="radio" name="ping" value="1"> Yes
                                    </label>
                                </div>
                            </div>
						</div>
						<div class="form-group">
                            {{ Form::label('dns', 'Disable DNS lookup for target?', ['class' => 'control-label col-xs-4']) }}
                            <div class="col-xs-8">
                                <div class="btn-group" data-toggle="buttons">
                                    <label class="btn btn-toggle active">
                                        <input type="radio" name="dns" id="dns" checked value="0"> No
                                    </label>
                                    <label class="btn btn-toggle">
                                        <input type="radio" name="dns" value="1"> Yes
                                    </label>
                                </div>
                            </div>
                        </div>
						<div class="form-group">
							{{ Form::label('os', 'Attempt to detect operating system?', ['class' => 'control-label col-xs-4']) }}
							<div class="col-xs-8">
                                <div class="btn-group" data-toggle="buttons">
                                    <label class="btn btn-toggle active">
                                        <input type="radio" name="os" id="os" checked value="0"> No
                                    </label>
                                    <label class="btn btn-toggle">
                                        <input type="radio" name="os" value="1"> Yes
                                    </label>
                                </div>
                            </div>
						</div>
						<hr/>
						<div class="form-group">
							{{ Form::label('email-me', 'Email me the results', ['class' => 'control-label col-xs-4']) }}
							<div class="col-xs-8">
                                <div class="btn-group" data-toggle="buttons">
                                    <label class="btn btn-toggle active">
                                        <input type="radio" name="email-me" id="email-me" checked value="0"> No
                                    </label>
                                    <label class="btn btn-toggle" data-toggle="tooltip" title="Feature coming soon">
                                        <input type="radio" name="email-me" value="1" disabled="disabled"> Yes
                                    </label>
                                </div>
                            </div>
						</div>
						<div class="form-group">
						    <div class="col-xs-offset-4 col-xs-8">
                                <div class="checkbox">
                                    <label><input type="checkbox" name="terms" value="1" checked="checked" required="required"> I understand and agree to the <a href="{{ url('terms-and-conditions', null, true) }}" title="Terms and Conditions">Terms &amp; Conditions</a></label>
                                </div>
                            </div>
                        </div>

						<div class="form-group ">
						    <div class="col-xs-offset-4 col-xs-8">
							    {{ Form::submit('Create scan', ['class' => 'btn btn-primary']) }}
							</div>
						</div>

					{{ Form::close() }}
					</div>
				</div> <!-- End default tab -->

				<div class="tab-pane fade" id="ssl">
				    <br/>
				    <p>Check for the Heartbleed Bug and weak ciphers</p>
				    <hr/>
					<div class="col-sm-12">
					{{ Form::open(['route' => 'console.nmap.store', 'class' => 'form-horizontal', 'role' => 'form']) }}
                        <br/>
                        @if(Session::has('message'))
                            <div class="alert alert-success" role="alert">
                                <p>{{ Session::get('message') }}</p>
                            </div><br />
                        @endif
						<div class="form-group">
							{{ Form::label('ssltarget', 'Target', ['class' => 'control-label col-xs-4']) }}
							<div class="col-sm-8">
							    <input type="text" name="target" id="ssltarget" class="form-control" required="required" data-toggle="tooltip" title="IPv4, IPv6 or hostname"/>
							</div>
						</div>
						{{ errors_for('ssltarget', $errors) }}
						<div class="form-group">
						    {{ Form::label('sslports', 'What ports do you want to scan?', ['class' => 'control-label col-xs-4']) }}
						    <div class="col-sm-7">
                                <select id="sslports" name="ports">
                                    <option value="top_100" class="">Top 100 ports</option>
                                    <option value="port_range">Port range (up to 100)</option>
                                    <option value="port_list" class="">Comma separated list of ports</option>
                                </select>
                            </div>
						</div>
                        <div id="sslport-list" class="form-group hidden">
                            <label for="port_comma_list" class="col-xs-4 control-label">Scan the following port list</label>
                            <div class="col-sm-8">
                                <textarea name="port_comma_list" class="form-control" data-toggle='tooltip' title='Comma separated list of ports'></textarea>
                            </div>
                        </div>
                        <div id="sslport-range" class="hidden">
                            <div class="form-group">
                                <label for="start_port" class="col-xs-4 control-label">Starting port</label>
                                <div class="col-xs-4">
                                    {{ Form::text('start_port', null,['class' => 'form-control', 'data-toggle' => 'tooltip', 'title' => 'Start port']) }}
                                </div>
                            </div>
                            <div class="form-group">
                                <label for="end_port" class="col-xs-4 control-label">Ending port</label>
                                <div class="col-xs-4">
                                    {{ Form::text('end_port', null,['class' => 'form-control', 'data-toggle' => 'tooltip', 'title' => 'End port']) }}
                                </div>
                            </div>
                        </div>
						<hr/>
						<div class="form-group">
							{{ Form::label('sslemail-me', 'Email me the results', ['class' => 'control-label col-xs-4']) }}
							<div class="btn-group" data-toggle="buttons">
                                <label class="btn btn-toggle active">
                                    <input type="radio" name="email-me" id="sslemail-me" checked value="0"> No
                                </label>
                                <label class="btn btn-toggle" data-toggle="tooltip" title="Feature coming soon">
                                    <input type="radio" name="email-me" value="1" disabled="disabled"> Yes
                                </label>
                            </div>
						</div>
						<div class="form-group">
                            <div class="col-xs-offset-4 col-xs-8">
                                <div class="checkbox">
                                    <label><input type="checkbox" name="terms" value="1" checked="checked" required="required"> I understand and agree to the <a href="{{ url('terms-and-conditions', null, true) }}" title="Terms and Conditions">Terms &amp; Conditions</a></label>
                                </div>
                            </div>
                        </div>
						<div class="form-group ">
                            <div class="col-xs-offset-4 col-xs-8">
                                {{ Form::hidden('scan_type', 'nmap_ssl') }}
                                {{ Form::submit('Create scan', ['class' => 'btn btn-primary']) }}
                            </div>
                        </div>

					{{ Form::close() }}

					</div>
				</div><!-- End SSL tab -->
			</div> <!-- End tabs -->
		</div>
	</div>

    <!-- Start second panel -->
    <div class="panel panel-primary col-sm-6" style="margin-top:20px;">
        <div class="panel-body">
            <h3 class="thin text-center">About the Nmap scan</h3>
            <hr>
            <p>Nmap is a very powerful port scanner that can scan both TCP and UDP ports. It also has the ability to do banner grabbing, which is like looking at a news story headline. You get back the information that tell you what’s inside. In the case of banner grabbing namp gets back the service version. Nmap also has a scripting engine that can turbo charge your scan into testing for all sorts security issues.</p>
            <h4>Scan type</h4>
            <p>This is either TCP or UDP. For more detailed information see <a href="https://en.wikibooks.org/wiki/Communication_Networks/TCP_and_UDP_Protocols" title="WikiBooks" target="_blank">WikiBooks</a>.</p>
            <h4>Target</h4>
            <p>This can either be an IPv4,IPv6 Address or hostname. If you give an IPv6 address we will add the correct Nmap flag.</p>
            <h4>Port scan technique</h4>
            <p>SYN scans is “SYN scan is the default and most popular scan option for good reasons. It can be performed quickly, scanning thousands of ports per second on a fast network not hampered by restrictive firewalls. It is also relatively unobtrusive and stealthy since it never completes TCP connections.</p>
            <p>TCP Connect scan is where a full connection is made.</p>
            <h4>Ports to scan</h4>
            <p><a href="http://insecure.org/fyodor/" title="Gordon Lyon" target="_blank">Gordon Lyon</a> scans the internet every year and compiles a list of the most common open ports. Nmap then uses this data for the top-ports option. For TCP you can scan for the most common 100,1000,2500 ports or if you want all the ports. UDP is limited to the top 100 ports because UDP scanning is very slow.</p>
            <h4>Services and scripts</h4>
            <p>Enumerate services versions is advanced banner grabbing.</p>
            <p>Nmap default scripts must be run in combination with services so that the scripting engine can use the information gained from the service enumeration.</p>
            <h4>Assume target is up?</h4>
            <p>If you run the scan and it says your host is down, then you want to make this Yes.</p>
            <h4>Disable DNS lookup for target?</h4>
            <p>This makes the scan faster as it disables DNS lookups.</p>
            <h4>Attempt to detect operating system?</h4>
            <p>Nmap can use traits it detects from the target to build up a likely operating system.</p>
            <h4>SSL check</h4>
            <p>Nmap will run all it’s SSL scripts. Checking for weak ciphers and vulnerabilities like heartbleed. Nmap scripts don’t show output unless it finds something bad.</p>
            <h4>Scan time</h4>
            <p>Scans time we vary depending on what options you choose. They should take no longer than 10 minutes.</p>
        </div>
    </div>
@stop
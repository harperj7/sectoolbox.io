@extends('backend/master')

@section("content")

	<div class="panel panel-primary" style="margin-top:20px;">
		<div class="panel-heading">Nmap scans</div>
		@if(Session::has('nmapmessage'))
			<div class="alert alert-info" role="alert">{{ Session::get('nmapmessage') }}</div>
		@endif
		<div class="table-responsive">

			<table class="table table-hover">
			<thead>
				<tr>
					<th>Target</th>
					<th>Status</th>
					<th>Requested</th>
					<th>Options</th>
					<th>Finished</th>
					<th>Duration</th>
					<th>View results</th>
					<th>Remove</th>
				</tr>
			</thead>
			<tbody>
				<?php $counter = 0; ?>
				@foreach ($nmap_scans as $scan)
					<tr>
						<td>{{{ $scan->target }}}</td>
						@if ($scan->completed == 0)
                            <td class="text-muted table-icon"><i class="fa fa-cogs" data-toggle="tooltip" title="Queued"></i></td>
                        @elseif ($scan->completed == 1)
                            <td class="text-info table-icon"><i class="fa fa-cog fa-spin" data-toggle="tooltip" title="In progress"></i></td>
                        @elseif($scan->completed == 2)
                            <td class="text-success table-icon"><i class="fa fa-check-circle-o" data-toggle="tooltip" title="Completed"></i></td>
                        @elseif($scan->completed == 3)
                            <td class="text-danger table-icon"><i class="fa fa-times-circle-o" data-toggle="tooltip" title="Failed"></i></td>
                        @endif
						<td>{{ $scan->created_at }}</td>
						<td>{{ $scan->options }}</td>
						@if ($scan->finished == '')
							<td>Not finished</td>
						@else
							<td>{{ $scan->finished }}</td>
						@endif
						@if (!empty($time_taken[$counter]))
							<td>{{ $time_taken[$counter] }}</td>
						@endif
						@if (!empty($scan->results))
							<td>
							<div class="btn-group">
								<a class="btn btn-success" href="{{ url("console/nmap/$scan->id/xml") }}" title="Download the raw XML output">XML</a>
								<a class="btn btn-success" href="{{ url("console/nmap/$scan->id/html") }}" target="_blank" title="View the HTML output">HTML</a>
							</div>
							</td>
						@else
							<td>Unvailable</td>
						@endif
						<td>
							<?php $scan_url = url("console/nmap/$scan->id") ?>
							{{ Form::open(array('url' => "$scan_url", 'method' => 'delete')) }}
								{{ Form::submit('Delete', ['class' => 'btn btn-danger btn-xs']) }}
							{{ Form::close() }}
						</td>
					</tr>
				<?php $counter++; ?>
				@endforeach
			</tbody>
		</table>
	</div>
		{{ $nmap_scans->links() }}
	</div>

@stop
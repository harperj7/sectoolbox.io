@extends('backend/master')

@section('content')

	<div class="panel panel-primary col-sm-12" style="margin-top:20px;">
		<div class="panel-body">

			<h3 class="thin text-center">Unauthorised access</h3>
			<hr>
			<p class="text-danger">Please go back to where you came from. You are not authorised to access this page.</p>
			<div style="width:135px;margin: 10px auto;"><img src="/assets/images/banana.gif" alt="Dancing banana" /></div>
        </div>
    </div>
@stop
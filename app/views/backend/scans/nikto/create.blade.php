@extends('backend/master')

@section('content')

	<div class="panel panel-primary col-sm-6" style="margin-top:20px;">
		<div class="panel-body">

			<h3 class="thin text-center">Create a new Nikto scan</h3>
			<hr>
			<div class="col-sm-12">
			@if(Session::has('message'))
                <div class="alert alert-success" role="alert">
                    <p>{{ Session::get('message') }}</p>
                </div><br />
            @endif
			{{ Form::open(['route' => 'console.nikto.store', 'class' => 'form-horizontal', 'role' => 'form']) }}
				<div class="form-group">
					{{ Form::label('target', 'Target', ['class' => 'control-label col-xs-4']) }}
					<div class="col-xs-8">
					    {{ Form::text('target', null, ['class' => 'form-control', 'required' => 'required']) }}
					</div>
				</div>
				{{ errors_for('target', $errors) }}
				<div class="form-group">
					{{ Form::label('port', 'Target port', ['class' => 'control-label col-xs-4']) }}
					<div class="col-xs-3">
					    {{ Form::text('port', null, ['class' => 'form-control']) }}
					</div>
				</div>
				{{ errors_for('port', $errors) }}
				<div class="form-group">
                    {{ Form::label('email-me', 'Email me the results', ['class' => 'control-label col-xs-4']) }}
                    <div class="col-xs-8">
                        <div class="btn-group" data-toggle="buttons">
                            <label class="btn btn-toggle active">
                                <input type="radio" name="email-me" id="email-me" checked value="0"> No
                            </label>
                            <label class="btn btn-toggle" data-toggle="tooltip" title="Feature coming soon">
                                <input type="radio" name="email-me" value="1" disabled="disabled"> Yes
                            </label>
                        </div>
                    </div>
                </div>
                <div class="form-group">
                    <div class="col-xs-offset-4 col-xs-8">
                        <div class="checkbox">
                            <label><input type="checkbox" name="terms" value="1" checked="checked" required="required"> I understand and agree to the <a href="{{ url('terms-and-conditions', null, true) }}" title="Terms and Conditions">Terms &amp; Conditions</a></label>
                        </div>
                    </div>
                </div>
				<div class="form-group ">
                    <div class="col-xs-offset-4 col-xs-8">
                        {{ Form::submit('Create scan', ['class' => 'btn btn-primary']) }}
                    </div>
                </div>

			{{ Form::close() }}
			</div>
		</div>
	</div>

	<!-- Start second panel -->
    <div class="panel panel-primary col-sm-6" style="margin-top:20px;">
        <div class="panel-body">

            <h3 class="thin text-center">About the Nikto scan</h3>
            <hr>
            <p>Nikto allows you to scan your website for insecure configurations and known vulnerabilities. It runs over 6200 tests against the website, reporting back any issues.</p>
            <h4>Target</h4>
            <p>This is the URL that Nikto will use a base for it’s scanning.<br />
            An example target URL is <code>http://example.com</code><br />
            If the website is using SSL you would enter <code>https://example.com</code></p>
            <h4>Target port</h4>
            <p>Optional. By default Nikto checks for the web server first on port 80 and then on port 443. If you're running the web server on a different port then set the target port to that port, otherwise leave it blank.</p>
            <h4>Scan time</h4>
            <p>Scans will take around 10 minutes against a fast server but up to 20 minutes against a slower server.</p>
        </div>

    </div>

@stop
@extends('backend/master')

@section('content')

	<div class="panel panel-primary" style="margin-top:20px;">
		<div class="panel-body">

			<h3 class="thin text-center">Create a new WPScan scan</h3>
			<hr>
			<div class="col-lg-6">
			{{ Form::open(['route' => 'console.wpscan.store']) }}

				<div class="form-group">
					{{ Form::label('target', 'Location of the Wordpress installation:') }}
					{{ Form::select('protocol', array('http' => 'http://', 'https' => 'https://'), 'http') }}
					{{ Form::text('target', null, ['class' => 'form-control', 'required' => 'required']) }}
					{{ errors_for('target', $errors) }}
				</div>
				<div class="form-group">
					{{ Form::label('email-me', 'Email me the results:') }} 
					<label class="radio-inline">
                        {{ Form::radio('email-me', '0', true) }}
                        No
                    </label>
                    <label class="radio-inline">
                        {{ Form::radio('email-me', '1', false) }}
                        Yes
                    </label>
				</div>

				<div>
					{{ Form::submit('Create scan', ['class' => 'btn btn-primary']) }}
				</div>

			{{ Form::close() }}
			@if(Session::has('message'))
				<br><div class="alert alert-success" role="alert">
				    <p>{{ Session::get('message') }}</p>
				</div>
			@endif
			</div>
		</div>
	</div>

@stop
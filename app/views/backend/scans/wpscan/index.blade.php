@extends('backend/master')

@section("content")

	<div class="panel panel-primary" style="margin-top:20px;">
		<div class="panel-heading">WPScan scans</div>
		@if(Session::has('message'))
			<div class="alert alert-info" role="alert">{{ Session::get('message') }}</div>
		@endif
		<div class="table-responsive">

			<table class="table table-hover">
			<thead>
				<tr>
					<th>Target</th>
					<th>Status</th>
					<th>Requested</th>
					<th>Port</th>
					<th>Started</th>
					<th>Finished</th>
					<th>Duration</th>
					<th>View results</th>
					<th>Remove</th>
				</tr>
			</thead>
			<tbody>
				<?php $counter = 0; ?>
				@foreach ($wpscan_scans as $scan)
					<tr>
						<td>{{ $scan->target }}</td>
						@if ($scan->completed == 0)
							<td class="alert-info">Incomplete</td>
						@else
							<td class="alert-success">Completed</td>
						@endif
						<td>{{ $scan->created_at }}</td>
						<td>{{ $scan->port }}</td>
						@if ($scan->started == '')
							<td>Not started</td>
						@else
							<td>{{ $scan->started }}</td>
						@endif
						@if ($scan->finished == '')
							<td>Not finished</td>
						@else
							<td>{{ $scan->finished }}</td>
						@endif
						@if (!empty($time_taken[$counter]))
							<td>{{ $time_taken[$counter] }}</td>
						@endif
						@if (!empty($scan->results))
							<td>
							<div class="btn-group">
								<a class="btn btn-success" href="{{ url("console/wpscan/$scan->id/xml") }}">XML</a><a class="btn btn-success" href="{{ url("console/wpscan/$scan->id/html") }}">HTML</a>
							</div>
							</td>
							
						@else
							<td>Unvailable</td>
						@endif
						<td>
							<?php $scan_url = url("console/wpscan/$scan->id") ?>
							{{ Form::open(array('url' => "$scan_url", 'method' => 'delete')) }}
								{{ Form::submit('Delete', ['class' => 'btn btn-danger btn-xs']) }}
							{{ Form::close() }}
						</td>
					</tr>
				<?php $counter++; ?>
				@endforeach
			</tbody>
		</table>
	</div>
		{{ $wpscan_scans->links() }}
	</div>
		

@stop
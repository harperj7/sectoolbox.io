@extends('backend/master')

@section('content')

	<div class="panel panel-primary col-sm-6" style="margin-top:20px;">
		<div class="panel-body">

			<h3 class="thin text-center">Create a new SQLMap scan</h3>
			<hr />
			<div class="col-lg-12">
			    {{ Form::open(['route' => 'console.sqlmap.store', 'class' => 'form-horizontal', 'role' => 'form']) }}
			    @if(Session::has('message'))
                    <div class="alert alert-success" role="alert">
                        <p>{{ Session::get('message') }}</p>
                    </div><br />
                @endif
				<div class="form-group">
					{{ Form::label('target', 'Target', ['class' => 'control-label col-xs-4']) }}
					<div class="col-xs-8">
					    {{ Form::text('target', null, ['class' => 'form-control', 'required' => 'required']) }}
					</div>
				</div>
				{{ errors_for('target', $errors) }}
				<div class="form-group">
                    {{ Form::label('port', 'Port', ['class' => 'control-label col-xs-4']) }}
                    <div class="col-xs-3">
                        {{ Form::text('port', null, ['class' => 'form-control']) }}
                    </div>
                </div>
                {{ errors_for('port', $errors) }}
                <div class="form-group">
                    {{ Form::label('test-forms', 'Scan type', ['class' => 'control-label col-xs-4']) }}
                    <div class="col-xs-5">
                        <select id="test-forms" name="test-forms" class="form-control">
                            <option value="0">GET SQLi check</option>
                            <option value="1">Test page forms</option>
                        </select>
                    </div>
                </div>
				<div class="form-group">
                    {{ Form::label('threads', 'Number of threads to use', ['class' => 'control-label col-xs-4']) }}
                    <div class="col-xs-3">
                        <select id="threads" name="threads" class="form-control">
                            <option value="2">2</option>
                            <option value="4" selected>4</option>
                            <option value="6">6</option>
                            <option value="8">8</option>
                            <option value="10">10</option>
                        </select>
                    </div>
                </div>
                <div class="form-group">
                    {{ Form::label('level', 'Level of tests to perform', ['class' => 'control-label col-xs-4']) }}
                    <div class="col-xs-3">
                        <select id="level" name="level" class="form-control">
                            <option value="1" selected>1</option>
                            <option value="2">2</option>
                            <option value="3">3</option>
                            <option value="4">4</option>
                            <option value="5">5</option>
                        </select>
                    </div>
                </div>
                <div class="form-group">
                    {{ Form::label('risk', 'Risk of tests to perform', ['class' => 'control-label col-xs-4']) }}
                    <div class="col-xs-3">
                        <select id="risk" name="risk" class="form-control">
                            <option value="1" selected>1</option>
                            <option value="2">2</option>
                            <option value="3">3</option>
                        </select>
                    </div>
                </div>
				<div class="form-group">
                    {{ Form::label('email-me', 'Email me the results', ['class' => 'control-label col-xs-4']) }}
                    <div class="col-xs-8">
                        <div class="btn-group" data-toggle="buttons">
                            <label class="btn btn-toggle active">
                                <input type="radio" name="email-me" id="email-me" checked value="0"> No
                            </label>
                            <label class="btn btn-toggle" data-toggle="tooltip" title="Feature coming soon">
                                <input type="radio" name="email-me" value="1" disabled="disabled"> Yes
                            </label>
                        </div>
                    </div>
                </div>
                <div class="form-group">
                    <div class="col-xs-offset-4 col-xs-8">
                        <div class="checkbox">
                            <label><input type="checkbox" name="terms" value="1" checked="checked" required="required"> I understand and agree to the <a href="{{ url('terms-and-conditions', null, true) }}" title="Terms and Conditions">Terms &amp; Conditions</a></label>
                        </div>
                    </div>
                </div>
                <div class="form-group ">
                    <div class="col-xs-offset-4 col-xs-8">
                        {{ Form::submit('Create scan', ['class' => 'btn btn-primary']) }}
                    </div>
                </div>

			{{ Form::close() }}
			</div>
		</div>
	</div>

	<!-- Start second panel -->
        <div class="panel panel-primary col-sm-6" style="margin-top:20px;">
            <div class="panel-body">
                <h3 class="thin text-center">About the SQLMap scan</h3>
                <hr>
                <p>SQLMap scans for sql injections present within a web application. If the injection is successful, SQLMap will attempt to retrieve the database banner to prove the injection is exploitable.</p>
                <h4>Target</h4>
                <p>This is the URL that SQLMap will test against. It accepts both <code>http://</code> and <code>https://</code>.</p>
                <h4>Target port</h4>
                <p>Optional. Specify a port when the web server is not running on port 80.</p>
                <h4>Scan type</h4>
                <p>GET SQLI checks GET parameters within the URL.</p>
                <p>Test page forms will look for forms on the web page and then test them. For example testing login forms.</p>
                <h4>Number of threads to use</h4>
                <p>This is the number of checks SQLMap will do simultaneously, the more threads the fast the scan completes and the more load it puts on your server.</p>
                <h4>Level</h4>
                <p>Level of tests to perform (1-5, default 1)</p>
                <h4>Risk</h4>
                <p>Risk of tests to perform (0-3, default 1)</p>
            </div>
        </div>

@stop
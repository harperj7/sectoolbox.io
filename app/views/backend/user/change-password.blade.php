@extends('backend/master')

@section('content')

	<div class="panel panel-primary">
	<!-- Default panel contents -->
	<div class="panel-heading">Change password</div>

        <div class="panel-body">

            {{ Form::open(['route' => 'update.password', 'method' => 'put', 'class' => 'form-horizontal', 'role' => 'form']) }}
                 @if(Session::has('message'))
                    <div class="alert alert-success" role="alert">
                        <p>{{ Session::get('message') }}</p>
                    </div><br />
                @endif
                <div class="form-group">
                    {{ Form::label('current-pass', 'Current password', ['class' => 'control-label col-xs-3']) }}
                    <div class="col-xs-9">
                        {{ Form::password('current-pass', null, ['class' => 'form-control', 'required' => 'required']) }}
                    </div>
                </div>
                {{ errors_for('current-pass', $errors) }}
                <div class="form-group">
                    {{ Form::label('password', 'New password', ['class' => 'control-label col-xs-3']) }}
                    <div class="col-xs-9">
                        {{ Form::password('password', null, ['class' => 'form-control', 'required' => 'required']) }}
                    </div>
                </div>
                {{ errors_for('password', $errors) }}
                <div class="form-group">
                    {{ Form::label('password_confirmation', 'New password again', ['class' => 'control-label col-xs-3']) }}
                    <div class="col-xs-9">
                        {{ Form::password('password_confirmation', null, ['class' => 'form-control', 'required' => 'required']) }}
                    </div>
                    {{ errors_for('password_confirmation', $errors) }}
                </div>
                <div class="form-group ">
                    <div class="col-xs-offset-3 col-xs-8">
                        {{ Form::submit('Change password', ['class' => 'btn btn-primary']) }}
                    </div>
                </div>

            {{ Form::close() }}

        </div>
	</div>

@stop
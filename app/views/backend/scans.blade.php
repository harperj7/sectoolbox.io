@extends('backend/master')

@section('content')

	<h1 class="page-header">Dashboard</h1>

	<div class="panel panel-primary">

	    <div class="panel-heading">Recent Nikto scans</div>

        @if(Session::has('niktomessage'))
            <div class="alert alert-info" role="alert">{{ Session::get('niktomessage') }}</div>
        @endif
        <div class="table-responsive">
            <table class="table table-hover">
            <thead>
                <tr>
                    <th>Target</th>
                    <th>Status</th>
                    <th>Requested</th>
                    <th>Port</th>
                    <th>Finished</th>
                    <th>Duration</th>
                    <th>View results</th>
                    <th>Remove</th>
                </tr>
            </thead>
            <tbody>
                <?php $counter = 0; ?>
                @foreach ($scans['nikto'] as $scan)
                    <tr>
                        <td>{{ $scan->target }}</td>
                        @if ($scan->completed == 0)
                            <td class="text-muted table-icon"><i class="fa fa-cogs" data-toggle="tooltip" title="Queued"></i></td>
                        @elseif ($scan->completed == 1)
                            <td class="text-info table-icon"><i class="fa fa-cog fa-spin" data-toggle="tooltip" title="In progress"></i></td>
                        @elseif($scan->completed == 2)
                            <td class="text-success table-icon"><i class="fa fa-check-circle-o" data-toggle="tooltip" title="Completed"></i></td>
                        @elseif($scan->completed == 3)
                            <td class="text-danger table-icon"><i class="fa fa-times-circle-o" data-toggle="tooltip" title="Failed"></i></td>
                        @endif
                        <td>{{ $scan->created_at }}</td>
                        <td>{{ $scan->port }}</td>
                        @if ($scan->finished == '')
                            <td>Not finished</td>
                        @else
                            <td>{{ $scan->finished }}</td>
                        @endif
                        @if (!empty($time_taken['nikto'][$counter]))
                            <td>{{ $time_taken['nikto'][$counter] }}</td>
                        @endif
                        @if (!empty($scan->results))
                            <td>
                            <div class="btn-group">
                                <a class="btn btn-success" href="{{ url("console/nikto/$scan->id/xml") }}">XML</a>
                            </div>
                            </td>
                        @else
                            <td>Unvailable</td>
                        @endif
                        <td>
                            <?php $scan_url = url("console/nikto/$scan->id") ?>
                            {{ Form::open(array('url' => "$scan_url", 'method' => 'delete')) }}
                                {{ Form::submit('Delete', ['class' => 'btn btn-danger btn-xs']) }}
                            {{ Form::close() }}
                        </td>
                    </tr>
                <?php $counter++; ?>
                @endforeach
            </tbody>
        </table>
	</div>

</div> <!-- End of recent Nikto scans panel -->

<div class="panel panel-primary" style="margin-top:5px;">
		<div class="panel-heading">Recent Nmap scans</div>
		@if(Session::has('nmapmessage'))
			<div class="alert alert-info" role="alert">{{ Session::get('nmapmessage') }}</div>
		@endif
		<div class="table-responsive">

			<table class="table table-hover">
			<thead>
				<tr>
					<th>Target</th>
					<th>Status</th>
					<th>Requested</th>
					<th>Options</th>
					<th>Finished</th>
					<th>Duration</th>
					<th>View results</th>
					<th>Remove</th>
				</tr>
			</thead>
			<tbody>
				<?php $counter = 0; ?>
				@foreach ($scans['nmap'] as $scan)
					<tr>
						<td>{{ $scan->target }}</td>
						@if ($scan->completed == 0)
                            <td class="text-muted table-icon"><i class="fa fa-cogs" data-toggle="tooltip" title="Queued"></i></td>
                        @elseif ($scan->completed == 1)
                            <td class="text-info table-icon"><i class="fa fa-cog fa-spin" data-toggle="tooltip" title="In progress"></i></td>
                        @elseif($scan->completed == 2)
                            <td class="text-success table-icon"><i class="fa fa-check-circle-o" data-toggle="tooltip" title="Completed"></i></td>
                        @elseif($scan->completed == 3)
                            <td class="text-danger table-icon"><i class="fa fa-times-circle-o" data-toggle="tooltip" title="Failed"></i></td>
                        @endif
						<td>{{ $scan->created_at }}</td>
						<td>{{ $scan->options }}</td>
						@if ($scan->finished == '')
							<td>Not finished</td>
						@else
							<td>{{ $scan->finished }}</td>
						@endif
						@if (!empty($time_taken['nmap'][$counter]))
							<td>{{ $time_taken['nmap'][$counter] }}</td>
						@endif
						@if (!empty($scan->results))
							<td>
							<div class="btn-group">
								<a class="btn btn-success" href="{{ url("console/nmap/$scan->id/xml") }}" title="Download the raw XML output">XML</a>
								<a class="btn btn-success" href="{{ url("console/nmap/$scan->id/html") }}" target="_blank" title="View the HTML output">HTML</a>
							</div>
							</td>
						@else
							<td>Unvailable</td>
						@endif
						<td>
							<?php $scan_url = url("console/nmap/$scan->id") ?>
							{{ Form::open(array('url' => "$scan_url", 'method' => 'delete')) }}
								{{ Form::submit('Delete', ['class' => 'btn btn-danger btn-xs']) }}
							{{ Form::close() }}
						</td>
					</tr>
				<?php $counter++; ?>
				@endforeach
			</tbody>
		</table>
	</div>
</div><!-- End of recent Nmap scans panel -->

<div class="panel panel-primary" style="margin-top:5px;">
    <div class="panel-heading">Recent OpenVAS scans</div>
    @if(Session::has('openvasmessage'))
        <div class="alert alert-info" role="alert">{{ Session::get('openvasmessage') }}</div>
    @endif
    <div class="table-responsive">

        <table class="table table-hover">
        <thead>
            <tr>
                <th>Target</th>
                <th>Status</th>
                <th>Requested</th>
                <th>Finished</th>
                <th>Duration</th>
                <th>View results</th>
                <th>Remove</th>
            </tr>
        </thead>
        <tbody>
            <?php $counter = 0; ?>
            @foreach ($scans['openvas'] as $scan)
                <tr>
                    <td>{{ $scan->target }}</td>
                    @if ($scan->completed == 0)
                        <td class="text-muted table-icon"><i class="fa fa-cogs" data-toggle="tooltip" title="Queued"></i></td>
                    @elseif ($scan->completed == 1)
                        <td class="text-info table-icon"><i class="fa fa-cog fa-spin" data-toggle="tooltip" title="In progress"></i></td>
                    @elseif($scan->completed == 2)
                        <td class="text-success table-icon"><i class="fa fa-check-circle-o" data-toggle="tooltip" title="Completed"></i></td>
                    @elseif($scan->completed == 3)
                        <td class="text-danger table-icon"><i class="fa fa-times-circle-o" data-toggle="tooltip" title="Failed"></i></td>
                    @endif
                    <td>{{ $scan->created_at }}</td>
                    @if ($scan->finished == '')
                        <td>Not finished</td>
                    @else
                        <td>{{ $scan->finished }}</td>
                    @endif
                    @if (!empty($time_taken['openvas'][$counter]))
                        <td>{{ $time_taken['openvas'][$counter] }}</td>
                    @endif
                    @if (!empty($scan->results))
                        <td>
                        <div class="btn-group">
                            <a class="btn btn-success" href="{{ url("console/openvas/$scan->id/xml") }}" title="Download the raw XML output">XML</a>
                            <a class="btn btn-success" href="{{ url("console/openvas/$scan->id/pdf") }}" title="Donwload the PDF output">PDF</a>
                            <a class="btn btn-success" href="{{ url("console/openvas/$scan->id/html") }}" target="_blank" title="View the HTML output">HTML</a>
                        </div>
                        </td>
                    @else
                        <td>Unvailable</td>
                    @endif
                    <td>
                        <?php $scan_url = url("console/openvas/$scan->id") ?>
                        {{ Form::open(array('url' => "$scan_url", 'method' => 'delete')) }}
                            {{ Form::submit('Delete', ['class' => 'btn btn-danger btn-xs']) }}
                        {{ Form::close() }}
                    </td>
                </tr>
            <?php $counter++; ?>
            @endforeach
            </tbody>
        </table>
    </div>
</div> <!-- End of recent OpenVAS scans panel -->

<div class="panel panel-primary" style="margin-top:5px;">
		<div class="panel-heading">Recent SQLMap scans</div>
		@if(Session::has('sqlmapmessage'))
			<div class="alert alert-info" role="alert">{{ Session::get('sqlmapmessage') }}</div>
		@endif
		<div class="table-responsive">

			<table class="table table-hover">
			<thead>
				<tr>
					<th>Target</th>
					<th>Status</th>
					<th>Requested</th>
					<th>Port</th>
					<th>Finished</th>
					<th>Duration</th>
					<th>View results</th>
					<th>Remove</th>
				</tr>
			</thead>
			<tbody>
				<?php $counter = 0; ?>
				@foreach ($scans['sqlmap'] as $scan)
					<tr>
						<td>{{ $scan->target }}</td>
						@if ($scan->completed == 0)
                            <td class="text-muted table-icon"><i class="fa fa-cogs" data-toggle="tooltip" title="Queued"></i></td>
                        @elseif ($scan->completed == 1)
                            <td class="text-info table-icon"><i class="fa fa-cog fa-spin" data-toggle="tooltip" title="In progress"></i></td>
                        @elseif($scan->completed == 2)
                            <td class="text-success table-icon"><i class="fa fa-check-circle-o" data-toggle="tooltip" title="Completed"></i></td>
                        @elseif($scan->completed == 3)
                            <td class="text-danger table-icon"><i class="fa fa-times-circle-o" data-toggle="tooltip" title="Failed"></i></td>
                        @endif
						<td>{{ $scan->created_at }}</td>
						<td>{{ $scan->port }}</td>
						@if ($scan->finished == '')
							<td>Not finished</td>
						@else
							<td>{{ $scan->finished }}</td>
						@endif
						@if (!empty($time_taken['sqlmap'][$counter]))
							<td>{{ $time_taken['sqlmap'][$counter] }}</td>
						@endif
						@if (!empty($scan->results))
							<td>
							<div class="btn-group">
								<a class="btn btn-success" href="{{ url("console/sqlmap/$scan->id/xml") }}" title="Download the raw XML output">XML</a>
							    <a class="btn btn-success" href="{{ url("console/sqlmap/$scan->id/html") }}" target="_blank" title="View the HTML output">HTML</a>
							</div>
							</td>

						@else
							<td>Unvailable</td>
						@endif
						<td>
							<?php $scan_url = url("console/sqlmap/$scan->id") ?>
							{{ Form::open(array('url' => "$scan_url", 'method' => 'delete')) }}
								{{ Form::submit('Delete', ['class' => 'btn btn-danger btn-xs']) }}
							{{ Form::close() }}
						</td>
					</tr>
				<?php $counter++; ?>
				@endforeach
			</tbody>
		</table>
	</div>
</div><!-- End of recent SQLMap scans panel -->

@stop
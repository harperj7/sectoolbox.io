<?php use Illuminate\Database\Eloquent\SoftDeletingTrait;

class Nmap extends \Eloquent {
	
	use SoftDeletingTrait;

	protected $table = 'nmap_scans';
	protected $fillable = [];
	protected $dates = ['deleted_at'];

    public function user() {
        return $this->belongsTo('user');
    }

}
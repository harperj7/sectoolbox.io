<?php

use Illuminate\Auth\UserTrait;
use Illuminate\Auth\UserInterface;
use Illuminate\Auth\Reminders\RemindableTrait;
use Illuminate\Auth\Reminders\RemindableInterface;
use Laravel\Cashier\BillableTrait;
use Laravel\Cashier\BillableInterface;

class User extends Eloquent implements UserInterface, RemindableInterface, BillableInterface {

	use UserTrait, RemindableTrait, BillableTrait;

	/**
	 * The database table used by the model.
	 *
	 * @var string
	 */
	protected $table = 'users';

	protected $fillable = [
		'email',
		'password'
	];

	/**
	 * The attributes excluded from the model's JSON form.
	 *
	 * @var array
	 */
	protected $hidden = array('password', 'remember_token');
	protected $dates = ['trial_ends_at', 'subscription_ends_at'];

	public function setPasswordAttribute($password) {

		$this->attributes['password'] = Hash::make($password);

	}

    public function nmap_scans() {
        return $this->hasMany('Nmap');
    }
    public function nikto_scans() {
        return $this->hasMany('Nikto');
    }
    public function openvas_scans() {
        return $this->hasMany('Openvas');
    }
    public function sqlmap_scans() {
        return $this->hasMany('Sqlmap');
    }
    public function scheduled_scans() {
        return $this->hasMany('ScheduledScans');
    }

}

<?php use Illuminate\Database\Eloquent\SoftDeletingTrait;

class Openvas extends \Eloquent {

	use SoftDeletingTrait;

	protected $table = 'openvas_scans';
	protected $fillable = [];
	protected $dates = ['deleted_at'];

    public function user() {
        return $this->belongsTo('user');
    }
}
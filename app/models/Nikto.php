<?php use Illuminate\Database\Eloquent\SoftDeletingTrait;

class Nikto extends \Eloquent {

	use SoftDeletingTrait;

	protected $table = 'nikto_scans';
	protected $fillable = [];
	protected $dates = ['deleted_at'];

    public function user() {
        return $this->belongsTo('user');
    }

}
<?php use Illuminate\Database\Eloquent\SoftDeletingTrait;

class Wpscan extends \Eloquent {
	
	use SoftDeletingTrait;

	protected $table = 'wpscan_scans';
	protected $fillable = [];
	protected $dates = ['deleted_at'];

}
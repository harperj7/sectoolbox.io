<?php use Illuminate\Database\Eloquent\SoftDeletingTrait;

class ScheduledScans extends \Eloquent {

    use SoftDeletingTrait;

	protected $fillable = [];
    protected $table = 'sqlmap_scans';
    protected $dates = ['deleted_at'];

    public function user() {
        return $this->belongsTo('user');
    }
}
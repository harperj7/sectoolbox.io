<?php use Illuminate\Database\Eloquent\SoftDeletingTrait;

class Sqlmap extends \Eloquent {
	
	use SoftDeletingTrait;

	protected $table = 'sqlmap_scans';
	protected $fillable = [];
	protected $dates = ['deleted_at'];

    public function user() {
        return $this->belongsTo('user');
    }
}
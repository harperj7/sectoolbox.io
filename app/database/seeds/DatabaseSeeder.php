<?php

class DatabaseSeeder extends Seeder {

	/**
	 * Run the database seeds.
	 *
	 * @return void
	 */
	public function run()
	{
		Eloquent::unguard();

		$this->call('NmapScansTableSeeder');
		$this->call('NiktoScansTableSeeder');
		$this->call('SqlmapScansTableSeeder');
		$this->call('WpscanScansTableSeeder');
		$this->call('OpenvasScansTableSeeder');
	}

}

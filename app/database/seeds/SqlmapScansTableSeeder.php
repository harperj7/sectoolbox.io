<?php

// Composer: "fzaninotto/faker": "v1.3.0"
use Faker\Factory as Faker;

class SqlmapScansTableSeeder extends Seeder {

	public function run()
	{
		$faker = Faker::create();

		foreach(range(1, 10) as $index)
		{
			Sqlmap::create([
				"user_id" => $faker->randomDigitNotNull,
				"target"  => $faker->domainName
			]);
		}
	}

}
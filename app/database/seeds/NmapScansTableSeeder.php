<?php

// Composer: "fzaninotto/faker": "v1.3.0"
use Faker\Factory as Faker;

class NmapScansTableSeeder extends Seeder {

	public function run()
	{
		$faker = Faker::create();

		foreach(range(1, 10) as $index)
		{
			Nmap::create([
				"user_id" => $faker->randomDigitNotNull,
				"target"  => $faker->domainName,
				"options" => "-sC -sV -O"
			]);
		}
	}

}
<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateSqlmapScansTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('sqlmap_scans', function(Blueprint $table)
		{
			$table->increments('id');
			$table->integer('user_id')->unsigned()->index();
			$table->string('target');
            $table->string('port')->nullable();
			$table->text('results')->nullable();
			$table->string('options');
            $table->text('message_sent');
			$table->boolean('completed')->default(0);
			$table->datetime('started')->nullable();
			$table->datetime('finished')->nullable();
			$table->text('changes')->nullable();
			$table->softDeletes();
			$table->timestamps();
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('sqlmap_scans');
	}

}

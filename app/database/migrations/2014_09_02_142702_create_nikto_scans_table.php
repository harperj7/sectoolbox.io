<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateNiktoScansTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('nikto_scans', function(Blueprint $table)
		{
			$table->increments('id');
			$table->integer('user_id')->unsigned()->index();
			$table->string('target');
            $table->text('message_sent');
			$table->string('results')->nullable();
			$table->string('port')->default('80, 443');
			$table->boolean('completed')->default(0);
            $table->text('changes')->nullable();
			$table->datetime('started')->nullable();
			$table->datetime('finished')->nullable();
			$table->softDeletes();
			$table->timestamps();
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::table('nikto_scans', function(Blueprint $table)
		{
			Schema::drop('nikto_scans');
		});
	}

}
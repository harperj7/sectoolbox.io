<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateScansScheduledTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('scans_scheduled', function(Blueprint $table)
		{
			$table->increments('id');
			$table->integer('scan_id')->unsigned()->index();
            $table->integer('user_id')->unsigned()->index();
			$table->string('type');
            $table->text('message');
			$table->dateTime('scheduled');
			$table->string('interval');
            $table->softDeletes();
            $table->timestamps();
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('scans_scheduled');
	}

}
